-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 30, 2020 at 06:46 PM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_terrazzo_site`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_commentmeta`
--

CREATE TABLE `tr_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tr_comments`
--

CREATE TABLE `tr_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_comments`
--

INSERT INTO `tr_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-04-08 17:29:32', '2020-04-08 20:29:32', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_links`
--

CREATE TABLE `tr_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tr_options`
--

CREATE TABLE `tr_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_options`
--

INSERT INTO `tr_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/terrazzo_site', 'yes'),
(2, 'home', 'http://localhost/projetos/terrazzo_site', 'yes'),
(3, 'blogname', 'Terrazzo', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'dev@gran.ag', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/index.php/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '7', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1601929749', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'tr_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'pt_BR', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:11:{i:1598826579;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1598862579;a:1:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1598862580;a:2:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1598891197;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1598905777;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1598905808;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1598905811;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1598906626;a:1:{s:21:\"ai1wm_storage_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1599150395;a:1:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1599164977;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1586379298;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(133, 'can_compress_scripts', '0', 'no'),
(138, 'recently_activated', 'a:0:{}', 'yes'),
(145, 'current_theme', 'Terrazzo', 'yes'),
(146, 'theme_mods_twentynineteen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1586379305;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(147, 'theme_switched', '', 'yes'),
(149, 'theme_mods_terrazzo', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(176, 'wpseo', 'a:20:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:6:\"13.4.1\";s:20:\"disableadvanced_meta\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1586449596;s:13:\"myyoast-oauth\";b:0;}', 'yes'),
(177, 'wpseo_titles', 'a:91:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:14:\"title-destaque\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-destaque\";s:0:\"\";s:16:\"noindex-destaque\";b:0;s:17:\"showdate-destaque\";b:0;s:27:\"display-metabox-pt-destaque\";b:1;s:27:\"post_types-destaque-maintax\";i:0;s:24:\"title-ptarchive-destaque\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-ptarchive-destaque\";s:0:\"\";s:26:\"bctitle-ptarchive-destaque\";s:0:\"\";s:26:\"noindex-ptarchive-destaque\";b:0;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:27:\"title-tax-categoriaservicos\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:30:\"metadesc-tax-categoriaservicos\";s:0:\"\";s:37:\"display-metabox-tax-categoriaservicos\";b:1;s:29:\"noindex-tax-categoriaservicos\";b:0;s:35:\"taxonomy-categoriaservicos-ptparent\";i:0;s:26:\"title-tax-categoriaprojeto\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:29:\"metadesc-tax-categoriaprojeto\";s:0:\"\";s:36:\"display-metabox-tax-categoriaprojeto\";b:1;s:28:\"noindex-tax-categoriaprojeto\";b:0;s:34:\"taxonomy-categoriaprojeto-ptparent\";i:0;}', 'yes'),
(178, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(179, 'wpseo_flush_rewrite', '1', 'yes'),
(185, 'wpseo_ryte', 'a:2:{s:6:\"status\";i:-1;s:10:\"last_fetch\";i:1586449599;}', 'yes'),
(188, 'rewrite_rules', 'a:126:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"index.php/destaque/?$\";s:28:\"index.php?post_type=destaque\";s:51:\"index.php/destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:46:\"index.php/destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:38:\"index.php/destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:57:\"index.php/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:45:\"index.php/category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:27:\"index.php/category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:54:\"index.php/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:49:\"index.php/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:30:\"index.php/tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:42:\"index.php/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:24:\"index.php/tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:55:\"index.php/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:50:\"index.php/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:31:\"index.php/type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:43:\"index.php/type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:25:\"index.php/type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:46:\"index.php/destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:56:\"index.php/destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:76:\"index.php/destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:71:\"index.php/destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:71:\"index.php/destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:52:\"index.php/destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"index.php/destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:39:\"index.php/destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:59:\"index.php/destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:54:\"index.php/destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:47:\"index.php/destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:54:\"index.php/destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:43:\"index.php/destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:35:\"index.php/destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"index.php/destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"index.php/destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"index.php/destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"index.php/destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"index.php/destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:69:\"index.php/categoria-servicos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaservicos=$matches[1]&feed=$matches[2]\";s:64:\"index.php/categoria-servicos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaservicos=$matches[1]&feed=$matches[2]\";s:45:\"index.php/categoria-servicos/([^/]+)/embed/?$\";s:50:\"index.php?categoriaservicos=$matches[1]&embed=true\";s:57:\"index.php/categoria-servicos/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaservicos=$matches[1]&paged=$matches[2]\";s:39:\"index.php/categoria-servicos/([^/]+)/?$\";s:39:\"index.php?categoriaservicos=$matches[1]\";s:69:\"index.php/categoria-projetos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?categoriaprojeto=$matches[1]&feed=$matches[2]\";s:64:\"index.php/categoria-projetos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?categoriaprojeto=$matches[1]&feed=$matches[2]\";s:45:\"index.php/categoria-projetos/([^/]+)/embed/?$\";s:49:\"index.php?categoriaprojeto=$matches[1]&embed=true\";s:57:\"index.php/categoria-projetos/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?categoriaprojeto=$matches[1]&paged=$matches[2]\";s:39:\"index.php/categoria-projetos/([^/]+)/?$\";s:38:\"index.php?categoriaprojeto=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:42:\"index.php/feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:37:\"index.php/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:18:\"index.php/embed/?$\";s:21:\"index.php?&embed=true\";s:30:\"index.php/page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:37:\"index.php/comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:51:\"index.php/comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:46:\"index.php/comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:27:\"index.php/comments/embed/?$\";s:21:\"index.php?&embed=true\";s:54:\"index.php/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:49:\"index.php/search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:30:\"index.php/search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:42:\"index.php/search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:24:\"index.php/search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:57:\"index.php/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:45:\"index.php/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:27:\"index.php/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:79:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:55:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:49:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:66:\"index.php/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:42:\"index.php/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:54:\"index.php/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:36:\"index.php/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:53:\"index.php/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:48:\"index.php/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:29:\"index.php/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:41:\"index.php/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:23:\"index.php/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:68:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:78:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:98:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:74:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:63:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:87:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:75:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:71:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:57:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:67:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:87:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:63:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:48:\"index.php/([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:37:\"index.php/.?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"index.php/.?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"index.php/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"index.php/.?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"index.php/(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:30:\"index.php/(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:50:\"index.php/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:45:\"index.php/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:38:\"index.php/(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:45:\"index.php/(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:34:\"index.php/(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(193, 'redux_version_upgraded_from', '3.6.18', 'yes'),
(195, 'configuracao', 'a:58:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:75:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/logo.png\";s:2:\"id\";s:2:\"11\";s:6:\"height\";s:2:\"68\";s:5:\"width\";s:3:\"284\";s:9:\"thumbnail\";s:82:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/logo-150x68.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:23:\"opt_texto_button_padrao\";s:21:\"Quero uma simulação\";s:22:\"opt_link_button_padrao\";s:1:\"#\";s:24:\"opt_titulo_secao_contato\";s:17:\"Deixe seu contato\";s:23:\"opt_texto_secao_contato\";s:175:\"Agora que você já conheceu um pouco mais sobre nosso lançamento, não deixe de entrar em contato com a nossa equipe para descobrir tudo que o Terrazzo Tomio tem a oferecer.\";s:16:\"opt_como_prefere\";s:23:\"Como prefere conversar?\";s:18:\"opt_telefone_icone\";a:9:{s:3:\"url\";s:79:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/telefone.svg\";s:2:\"id\";s:2:\"13\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:76:\"http://localhost/projetos/terrazzo_site/wp-includes/images/media/default.png\";s:5:\"title\";s:8:\"telefone\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:12:\"opt_telefone\";s:12:\"287-579-3100\";s:15:\"opt_email_icone\";a:9:{s:3:\"url\";s:76:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/email.svg\";s:2:\"id\";s:2:\"12\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:76:\"http://localhost/projetos/terrazzo_site/wp-includes/images/media/default.png\";s:5:\"title\";s:5:\"email\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:9:\"opt_email\";s:19:\"terrazzo@atr.com.br\";s:22:\"opt_videochamada_icone\";a:9:{s:3:\"url\";s:80:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/videocall.svg\";s:2:\"id\";s:2:\"14\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:76:\"http://localhost/projetos/terrazzo_site/wp-includes/images/media/default.png\";s:5:\"title\";s:9:\"videocall\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:23:\"opt_videochamada_titulo\";s:22:\"Agendar vídeo chamada\";s:21:\"opt_videochamada_link\";s:1:\"#\";s:18:\"opt_whatsapp_icone\";a:9:{s:3:\"url\";s:74:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/zap.svg\";s:2:\"id\";s:2:\"15\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:76:\"http://localhost/projetos/terrazzo_site/wp-includes/images/media/default.png\";s:5:\"title\";s:3:\"zap\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"opt_whatsapp_titulo\";s:8:\"Whatsapp\";s:19:\"opt_whatsapp_numero\";s:13:\"5541991234567\";s:25:\"opt_titulo_secao_destaque\";s:40:\"O primeiro prédio inteligente da cidade\";s:28:\"opt_descricao_secao_destaque\";s:149:\"Além de muita tecnologia, democratizamos a ambiência das coberturas através do conceito de terrazzos privativos em um condomínio repleto de lazer\";s:24:\"opt_video_secao_destaque\";s:85:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/ATR-Valparaiso.mp4\";s:31:\"opt_imagem_video_secao_destaque\";a:9:{s:3:\"url\";s:82:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/bannervideo.png\";s:2:\"id\";s:2:\"42\";s:6:\"height\";s:3:\"826\";s:5:\"width\";s:4:\"1413\";s:9:\"thumbnail\";s:90:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/bannervideo-150x150.png\";s:5:\"title\";s:11:\"bannervideo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:24:\"opt_titulo_ficha_tecnica\";s:14:\"Ficha técnica\";s:24:\"opt_ficha_tecnica_item_1\";s:13:\"+ de 8.000 m2\";s:31:\"opt_ficha_tecnica_item_1_titulo\";s:17:\"Área construída\";s:24:\"opt_ficha_tecnica_item_2\";s:2:\"17\";s:31:\"opt_ficha_tecnica_item_2_titulo\";s:10:\"Pavimentos\";s:24:\"opt_ficha_tecnica_item_3\";s:3:\"54m\";s:31:\"opt_ficha_tecnica_item_3_titulo\";s:9:\"de altura\";s:24:\"opt_ficha_tecnica_item_4\";s:2:\"93\";s:31:\"opt_ficha_tecnica_item_4_titulo\";s:8:\"Unidades\";s:25:\"opt_titulo_secao_terrazzo\";s:57:\"Prático e Inteligente. Perfeito para receber os amigos. \";s:28:\"opt_subtitulo_secao_terrazzo\";s:28:\"Perfeito para receber amigos\";s:24:\"opt_texto_secao_terrazzo\";s:521:\"Muito espaço ao ar livre na área mais nobre de São José dos Pinhais, na esquina das ruas Maria Helena com Cap. Tobias Pereira da Cruz, próximo a supermercados, academias, escolas, farmácias e hospitais, o Terrazzo Tomio propõem um estilo de vida moderno e futurista, mesclando natureza com tecnologia. <strong>Viva o futuro.</strong> Acesse por reconhecimento facial as principais entradas do prédio e usufrua da vista da serra do mar em um elevador panorâmico que gera energia fotovoltaica pelo próprio vidro.\";s:24:\"opt_titulo_secao_plantas\";s:14:\"Nossas plantas\";s:23:\"opt_texto_secao_plantas\";s:348:\"O Terrazzo Tomio é a oportunidade perfeita para você̂ viver muito bem localizado em um apartamento de alto padrão. Opções de plantas compactas de 52m2 com terrazzos privativos de até 24m2 com pé́ direito duplo. E também opções de plantas unificadas de até 214 m2 privativos ou coberturas duplex invertidas com preparação para SPA.\";s:28:\"opt_titulo_secao_localizacao\";s:13:\"Localização\";s:27:\"opt_texto_secao_localizacao\";s:190:\"O bairro São Pedro tem toda a comodidade do centro em uma das vizinhanças mais sofisticadas da cidade. Você será vizinho de mais de vinte opções de lazer, cultura, saúde e educação.\";s:36:\"opt_titulo_estabelecimentos_proximos\";s:26:\"Estabelecimentos próximos\";s:47:\"opt_estabelecimentos_proximos_secao_localizacao\";a:6:{i:0;s:13:\"Supermercados\";i:1;s:8:\"Padarias\";i:2;s:9:\"Academias\";i:3;s:7:\"Escolas\";i:4;s:10:\"Farmácias\";i:5;s:8:\"Hospital\";}s:26:\"opt_link_secao_localizacao\";s:234:\"https://www.google.com/maps/place/R.+Maria+Helena,+491+-+S%C3%A3o+Pedro,+S%C3%A3o+Jos%C3%A9+dos+Pinhais+-+PR,+83005-480/@-25.5470432,-49.1937365,17z/data=!4m5!3m4!1s0x94dcf751fa21b3df:0x28b94fe524a718a5!8m2!3d-25.5467141!4d-49.1935541\";s:29:\"opt_titulo_secao_diferenciais\";s:21:\"Diferenciais Terrazzo\";s:32:\"opt_subtitulo_secao_diferenciais\";s:50:\"Aqui você vive suas escolhas. E podem ser muitas!\";s:28:\"opt_texto_secao_diferenciais\";s:834:\"Sala de reuniões para encontrar com clientes, academia para cuidar da forma, quadra e playground para a criançada brincar, piscina com borda infinita que proporcionará fotos de dar inveja, salão de festas para receber os amigos e muito mais.</p>\r\n\r\n<p>Um prédio tecnológico e futurista! Não deu tempo de fazer compras?! Tudo bem, peça por um aplicativo, libere o acesso do entregador e deixe que suas compras te aguardem no freezer do <strong>delivery space</strong>. Faltou algum ingrediente para o prato do jantar? Muito provavelmente você o encontre na <strong>vending machine</strong> que se encontra no lobby.</p>\r\n\r\n<p>Faca uma aula de ioga no seu terrazzo privativo ou em nosso espaço zen.  Escolha a comodidade que só no Terrazzo Tomio você terá. O primeiro prédio inteligente da cidade é da ATR Incorporadora.\";s:35:\"opt_imagem_celular_secao_aplicativo\";a:9:{s:3:\"url\";s:78:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/celular.png\";s:2:\"id\";s:2:\"16\";s:6:\"height\";s:4:\"1602\";s:5:\"width\";s:3:\"910\";s:9:\"thumbnail\";s:86:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/celular-150x150.png\";s:5:\"title\";s:7:\"celular\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:27:\"opt_titulo_secao_aplicativo\";s:22:\"Baixe <br>o aplicativo\";s:26:\"opt_texto_secao_aplicativo\";s:401:\"A ATR sempre trouxe a tecnologia para seus empreendimentos e agora chegou a vez de ir além: um prédio com um sistema próprio. Desenvolvemos um aplicativo que controla os equipamentos de acesso e monitoramento instalados no prédio. Além de gerir as áreas de lazer e a comunicação do condomínio, ainda facilita as demandas de assistência técnica e gestão de manuais e projetos de engenharia.\";s:19:\"opt_icone_app_store\";a:9:{s:3:\"url\";s:79:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/appstore.png\";s:2:\"id\";s:2:\"17\";s:6:\"height\";s:3:\"123\";s:5:\"width\";s:3:\"360\";s:9:\"thumbnail\";s:87:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/appstore-150x123.png\";s:5:\"title\";s:8:\"appstore\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:18:\"opt_link_app_store\";s:1:\"#\";s:20:\"opt_icone_play_store\";a:9:{s:3:\"url\";s:80:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/playstore.png\";s:2:\"id\";s:2:\"18\";s:6:\"height\";s:3:\"120\";s:5:\"width\";s:3:\"402\";s:9:\"thumbnail\";s:88:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/playstore-150x120.png\";s:5:\"title\";s:9:\"playstore\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"opt_link_play_store\";s:1:\"#\";s:26:\"opt_titulo_secao_sobre_atr\";s:48:\"ATR é mais inovação e tecnologia para você!\";s:29:\"opt_subtitulo_secao_sobre_atr\";s:82:\"Com foco na inovação, entregamos apartamentos cheios de criatividade e conforto.\";s:25:\"opt_texto_secao_sobre_atr\";s:481:\"Somos conhecidos pelo alto padrão de acabamento, nível de inovação e fachadas arrojadas. Associados ao Sinduscon-PR, registrados junto ao Crea-PR (57679), certificados nível A no PBQP-h e ISO9001:2015. Mas principalmente somos entusiastas de novas tecnologias que possam poupar recursos naturais, industrializar métodos executivos e aumentar a qualidade de vida nos apartamentos que construímos. Muito além de um empreendimento, entregamos momentos de alegria e felicidade.\";s:25:\"opt_lista_secao_sobre_atr\";a:4:{i:0;s:31:\"Fachadas com designs exclusivos\";i:1;s:33:\"Os mais modernos itens tecnologia\";i:2;s:36:\"As melhores condições de pagamento\";i:3;s:28:\"Opções de personalização\";}s:28:\"opt_texto_button_conheca_atr\";s:14:\"Conheça a atr\";s:22:\"opt_button_conheca_atr\";s:32:\"https://atrincorporadora.com.br/\";s:26:\"opt_imagem_secao_sobre_atr\";a:9:{s:3:\"url\";s:75:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/eric.png\";s:2:\"id\";s:2:\"19\";s:6:\"height\";s:4:\"1486\";s:5:\"width\";s:3:\"964\";s:9:\"thumbnail\";s:83:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/eric-150x150.png\";s:5:\"title\";s:4:\"eric\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(196, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:2:{s:8:\"last_tab\";s:2:\"15\";s:25:\"opt_titulo_secao_terrazzo\";s:22:\"PRÁTICO E INTELIGENTE\";}s:9:\"last_save\";i:1598796569;}', 'yes'),
(204, 'r_notice_data', '{}', 'yes'),
(205, 'redux_blast', '1586790918', 'yes'),
(230, 'recovery_mode_email_last_sent', '1586797649', 'yes'),
(245, 'duplicate_post_copytitle', '1', 'yes'),
(246, 'duplicate_post_copydate', '', 'yes'),
(247, 'duplicate_post_copystatus', '', 'yes'),
(248, 'duplicate_post_copyslug', '', 'yes'),
(249, 'duplicate_post_copyexcerpt', '1', 'yes'),
(250, 'duplicate_post_copycontent', '1', 'yes'),
(251, 'duplicate_post_copythumbnail', '1', 'yes'),
(252, 'duplicate_post_copytemplate', '1', 'yes'),
(253, 'duplicate_post_copyformat', '1', 'yes'),
(254, 'duplicate_post_copyauthor', '', 'yes'),
(255, 'duplicate_post_copypassword', '', 'yes'),
(256, 'duplicate_post_copyattachments', '', 'yes'),
(257, 'duplicate_post_copychildren', '', 'yes'),
(258, 'duplicate_post_copycomments', '', 'yes'),
(259, 'duplicate_post_copymenuorder', '1', 'yes'),
(260, 'duplicate_post_taxonomies_blacklist', '', 'yes'),
(261, 'duplicate_post_blacklist', '', 'yes'),
(262, 'duplicate_post_types_enabled', 'a:4:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:6:\"planta\";i:3;s:5:\"sobre\";}', 'yes'),
(263, 'duplicate_post_show_row', '1', 'yes'),
(264, 'duplicate_post_show_adminbar', '1', 'yes'),
(265, 'duplicate_post_show_submitbox', '1', 'yes'),
(266, 'duplicate_post_show_bulkactions', '1', 'yes'),
(267, 'duplicate_post_show_original_column', '', 'yes'),
(268, 'duplicate_post_show_original_in_post_states', '', 'yes'),
(269, 'duplicate_post_show_original_meta_box', '', 'yes'),
(270, 'duplicate_post_version', '3.2.4', 'yes'),
(271, 'duplicate_post_show_notice', '', 'no'),
(273, 'duplicate_post_title_prefix', '', 'yes'),
(274, 'duplicate_post_title_suffix', '', 'yes'),
(275, 'duplicate_post_increase_menu_order_by', '', 'yes'),
(276, 'duplicate_post_roles', 'a:2:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";}', 'yes'),
(316, 'category_children', 'a:0:{}', 'yes'),
(327, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:8:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"wp_block\";s:4:\"show\";s:8:\"destaque\";s:4:\"show\";s:6:\"planta\";s:4:\"show\";s:11:\"diferencial\";s:4:\"show\";s:7:\"galeria\";s:4:\"show\";s:5:\"sobre\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(328, 'CPT_configured', 'TRUE', 'yes'),
(335, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.8\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1587141773;s:7:\"version\";s:5:\"5.1.7\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(351, 'ai1wm_updater', 'a:0:{}', 'yes'),
(411, 'jetpack_active_modules', 'a:0:{}', 'yes'),
(430, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:11:\"dev@gran.ag\";s:7:\"version\";s:5:\"5.4.1\";s:9:\"timestamp\";i:1588248644;}', 'no'),
(715, 'active_plugins', 'a:6:{i:0;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";i:1;s:35:\"redux-framework/redux-framework.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:33:\"duplicate-post/duplicate-post.php\";i:4;s:21:\"meta-box/meta-box.php\";i:5;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(716, 'ai1wm_secret_key', '7BljGS2FN6Kd', 'yes'),
(718, 'ai1wm_backups_labels', 'a:0:{}', 'yes'),
(719, 'ai1wm_sites_links', 'a:0:{}', 'yes'),
(720, 'ai1wm_status', 'a:3:{s:4:\"type\";s:4:\"done\";s:5:\"title\";s:41:\"Your site has been imported successfully!\";s:7:\"message\";s:392:\"» <a class=\"ai1wm-no-underline\" href=\"http://localhost/projetos/terrazzo_site/wp-admin/options-permalink.php#submit\" target=\"_blank\">Save permalinks structure</a>. (opens a new window)<br />» <a class=\"ai1wm-no-underline\" href=\"https://wordpress.org/support/view/plugin-reviews/all-in-one-wp-migration?rate=5#postform\" target=\"_blank\">Optionally, review the plugin</a>. (opens a new window)\";}', 'yes'),
(723, '_site_transient_timeout_php_check_8445f3f6d5c32e543da408dfa7e06e48', '1599010156', 'no'),
(724, '_site_transient_php_check_8445f3f6d5c32e543da408dfa7e06e48', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(725, '_transient_health-check-site-status-result', '{\"good\":\"9\",\"recommended\":\"7\",\"critical\":\"1\"}', 'yes'),
(735, 'template', 'terrazzo', 'yes'),
(736, 'stylesheet', 'terrazzo', 'yes'),
(751, '_site_transient_timeout_browser_5473f3a4ae341429fd5dae299d69bac4', '1599096203', 'no'),
(752, '_site_transient_browser_5473f3a4ae341429fd5dae299d69bac4', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"84.0.4147.135\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(771, '_transient_timeout_select2-css_style_cdn_is_up', '1598882943', 'no'),
(772, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(773, '_transient_timeout_select2-js_script_cdn_is_up', '1598882944', 'no'),
(774, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(786, '_site_transient_timeout_theme_roots', '1598821187', 'no'),
(787, '_site_transient_theme_roots', 'a:2:{s:8:\"terrazzo\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";}', 'no');
INSERT INTO `tr_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(790, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.5.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.5.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.5\";s:7:\"version\";s:3:\"5.5\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.5.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.5.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.5-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.5-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.5\";s:7:\"version\";s:3:\"5.5\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.5.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.5.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.5\";s:7:\"version\";s:3:\"5.5\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1598819392;s:15:\"version_checked\";s:3:\"5.4\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.4\";s:7:\"updated\";s:19:\"2020-04-24 18:28:53\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(791, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1598819393;s:7:\"checked\";a:2:{s:8:\"terrazzo\";s:5:\"1.0.0\";s:14:\"twentynineteen\";s:3:\"1.5\";}s:8:\"response\";a:1:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.7.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:14:\"twentynineteen\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"1.5\";s:7:\"updated\";s:19:\"2020-04-01 11:57:15\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/theme/twentynineteen/1.5/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(792, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1598819395;s:7:\"checked\";a:10:{s:19:\"akismet/akismet.php\";s:5:\"4.1.5\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:4:\"7.21\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.8\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.4\";s:9:\"hello.php\";s:5:\"1.7.2\";s:21:\"meta-box/meta-box.php\";s:5:\"5.3.0\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.4.3\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:5:\"3.3.2\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.18\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"14.1\";}s:8:\"response\";a:9:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"7.26\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.26.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2246309\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2246309\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2246309\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2246309\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";s:6:\"5.2.17\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.2.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.2.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:80:\"<p>First release from Yoast + accessibility improvements + filter deprecated</p>\";s:6:\"tested\";s:5:\"5.4.2\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:5:\"5.3.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/meta-box.5.3.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.2\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.5.2\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.5.2.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:31:\"w.org/plugins/really-simple-ssl\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:6:\"plugin\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:11:\"new_version\";s:5:\"3.3.5\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/really-simple-ssl/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/really-simple-ssl.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/really-simple-ssl/assets/icon-128x128.png?rev=1782452\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/really-simple-ssl/assets/banner-1544x500.png?rev=2320223\";s:2:\"1x\";s:72:\"https://ps.w.org/really-simple-ssl/assets/banner-772x250.png?rev=2320228\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"4.1.17\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.4.1.17.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:68:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=2352112\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/redux-framework/assets/banner-1544x500.png?rev=2352114\";s:2:\"1x\";s:70:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=2352114\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:6:\"14.8.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wordpress-seo.14.8.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:3:\"5.5\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:8:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:7:\"akismet\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.1.5\";s:7:\"updated\";s:19:\"2020-04-10 06:41:44\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/translation/plugin/akismet/4.1.5/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:4:\"7.21\";s:7:\"updated\";s:19:\"2020-05-01 18:58:12\";s:7:\"package\";s:89:\"https://downloads.wordpress.org/translation/plugin/all-in-one-wp-migration/7.21/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.1.8\";s:7:\"updated\";s:19:\"2020-04-10 16:37:58\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.1.8/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"duplicate-post\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.2.4\";s:7:\"updated\";s:19:\"2020-01-30 18:54:04\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.2.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"hello-dolly\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"1.7.2\";s:7:\"updated\";s:19:\"2019-08-13 18:09:11\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/hello-dolly/1.7.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:5;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.3.0\";s:7:\"updated\";s:19:\"2019-07-13 02:22:41\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/plugin/meta-box/5.3.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:6;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.3.2\";s:7:\"updated\";s:19:\"2020-04-10 06:35:44\";s:7:\"package\";s:84:\"https://downloads.wordpress.org/translation/plugin/really-simple-ssl/3.3.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:7;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:4:\"14.1\";s:7:\"updated\";s:19:\"2020-05-07 20:52:26\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/14.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:1:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `tr_postmeta`
--

CREATE TABLE `tr_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_postmeta`
--

INSERT INTO `tr_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(9, 7, '_edit_lock', '1586381071:1'),
(10, 7, '_wp_page_template', 'pages/inicial.php'),
(13, 11, '_wp_attached_file', '2020/04/logo.png'),
(14, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:284;s:6:\"height\";i:68;s:4:\"file\";s:16:\"2020/04/logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-150x68.png\";s:5:\"width\";i:150;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(15, 12, '_wp_attached_file', '2020/04/email.svg'),
(16, 13, '_wp_attached_file', '2020/04/telefone.svg'),
(17, 14, '_wp_attached_file', '2020/04/videocall.svg'),
(18, 15, '_wp_attached_file', '2020/04/zap.svg'),
(19, 16, '_wp_attached_file', '2020/04/celular.png'),
(20, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:910;s:6:\"height\";i:1602;s:4:\"file\";s:19:\"2020/04/celular.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"celular-170x300.png\";s:5:\"width\";i:170;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"celular-582x1024.png\";s:5:\"width\";i:582;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"celular-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"celular-768x1352.png\";s:5:\"width\";i:768;s:6:\"height\";i:1352;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:20:\"celular-873x1536.png\";s:5:\"width\";i:873;s:6:\"height\";i:1536;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 17, '_wp_attached_file', '2020/04/appstore.png'),
(22, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:123;s:4:\"file\";s:20:\"2020/04/appstore.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"appstore-300x103.png\";s:5:\"width\";i:300;s:6:\"height\";i:103;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"appstore-150x123.png\";s:5:\"width\";i:150;s:6:\"height\";i:123;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(23, 18, '_wp_attached_file', '2020/04/playstore.png'),
(24, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:402;s:6:\"height\";i:120;s:4:\"file\";s:21:\"2020/04/playstore.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"playstore-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"playstore-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 19, '_wp_attached_file', '2020/04/eric.png'),
(26, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:964;s:6:\"height\";i:1486;s:4:\"file\";s:16:\"2020/04/eric.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"eric-195x300.png\";s:5:\"width\";i:195;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"eric-664x1024.png\";s:5:\"width\";i:664;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"eric-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"eric-768x1184.png\";s:5:\"width\";i:768;s:6:\"height\";i:1184;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(27, 20, '_wp_attached_file', '2020/04/video1.png'),
(28, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1487;s:6:\"height\";i:826;s:4:\"file\";s:18:\"2020/04/video1.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"video1-300x167.png\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"video1-1024x569.png\";s:5:\"width\";i:1024;s:6:\"height\";i:569;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"video1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"video1-768x427.png\";s:5:\"width\";i:768;s:6:\"height\";i:427;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 29, '_edit_last', '1'),
(30, 29, '_edit_lock', '1598802712:1'),
(31, 30, '_wp_attached_file', '2020/04/cama.svg'),
(32, 31, '_wp_attached_file', '2020/04/carro.svg'),
(33, 32, '_wp_attached_file', '2020/04/fingerprint.svg'),
(34, 33, '_wp_attached_file', '2020/04/gas.svg'),
(35, 34, '_wp_attached_file', '2020/04/metragem.svg'),
(36, 35, '_wp_attached_file', '2020/04/mute.svg'),
(37, 36, '_wp_attached_file', '2020/04/underfloor.svg'),
(38, 37, '_wp_attached_file', '2020/04/planta.png'),
(39, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:809;s:6:\"height\";i:483;s:4:\"file\";s:18:\"2020/04/planta.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"planta-300x179.png\";s:5:\"width\";i:300;s:6:\"height\";i:179;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"planta-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"planta-768x459.png\";s:5:\"width\";i:768;s:6:\"height\";i:459;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 29, 'terrazzo_metragem_planta', '128m² área privativa'),
(43, 29, 'terrazzo_diferenciais_planta', 'a:4:{i:0;a:2:{s:5:\"icone\";s:75:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/mute.svg\";s:11:\"diferencial\";s:19:\"Vedação Acústica\";}i:1;a:2:{s:5:\"icone\";s:81:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/underfloor.svg\";s:11:\"diferencial\";s:13:\"Piso Aquecido\";}i:2;a:2:{s:5:\"icone\";s:74:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/gas.svg\";s:11:\"diferencial\";s:18:\"Aquecimento a Gás\";}i:3;a:2:{s:5:\"icone\";s:82:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/fingerprint.svg\";s:11:\"diferencial\";s:21:\"Fechadura Biométrica\";}}'),
(44, 29, 'terrazzo_detalhes_sobre_planta', 'a:3:{i:0;a:2:{s:5:\"icone\";s:79:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/metragem.svg\";s:7:\"detalhe\";s:25:\"128m² de área privativa\";}i:1;a:2:{s:5:\"icone\";s:75:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/cama.svg\";s:7:\"detalhe\";s:13:\"02-03 quartos\";}i:2;a:2:{s:5:\"icone\";s:76:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/carro.svg\";s:7:\"detalhe\";s:22:\"01-02 vagas de garagem\";}}'),
(47, 39, 'terrazzo_metragem_planta', '96m² área privativa'),
(48, 39, 'terrazzo_diferenciais_planta', 'a:4:{i:0;a:2:{s:5:\"icone\";s:82:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/fingerprint.svg\";s:11:\"diferencial\";s:21:\"Fechadura Biométrica\";}i:1;a:2:{s:5:\"icone\";s:74:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/gas.svg\";s:11:\"diferencial\";s:18:\"Aquecimento a Gás\";}i:2;a:2:{s:5:\"icone\";s:75:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/mute.svg\";s:11:\"diferencial\";s:19:\"Vedação Acústica\";}i:3;a:2:{s:5:\"icone\";s:81:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/underfloor.svg\";s:11:\"diferencial\";s:13:\"Piso Aquecido\";}}'),
(49, 39, 'terrazzo_detalhes_sobre_planta', 'a:3:{i:0;a:2:{s:5:\"icone\";s:79:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/metragem.svg\";s:7:\"detalhe\";s:24:\"96m² de área privativa\";}i:1;a:2:{s:5:\"icone\";s:75:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/cama.svg\";s:7:\"detalhe\";s:10:\"02 quartos\";}i:2;a:2:{s:5:\"icone\";s:76:\"http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/carro.svg\";s:7:\"detalhe\";s:19:\"01 vagas de garagem\";}}'),
(52, 39, '_dp_original', '29'),
(53, 39, '_edit_last', '1'),
(54, 39, '_edit_lock', '1598802712:1'),
(67, 42, '_wp_attached_file', '2020/04/bannervideo.png'),
(68, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1413;s:6:\"height\";i:826;s:4:\"file\";s:23:\"2020/04/bannervideo.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"bannervideo-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"bannervideo-1024x599.png\";s:5:\"width\";i:1024;s:6:\"height\";i:599;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"bannervideo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"bannervideo-768x449.png\";s:5:\"width\";i:768;s:6:\"height\";i:449;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(69, 44, '_edit_last', '1'),
(70, 44, '_edit_lock', '1587141630:1'),
(71, 45, '_wp_attached_file', '2020/04/g.png'),
(72, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1199;s:6:\"height\";i:544;s:4:\"file\";s:13:\"2020/04/g.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"g-300x136.png\";s:5:\"width\";i:300;s:6:\"height\";i:136;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:14:\"g-1024x465.png\";s:5:\"width\";i:1024;s:6:\"height\";i:465;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"g-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:13:\"g-768x348.png\";s:5:\"width\";i:768;s:6:\"height\";i:348;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(73, 46, '_wp_attached_file', '2020/04/g1.png'),
(74, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g1-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(75, 47, '_wp_attached_file', '2020/04/g2.png'),
(76, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g2-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(77, 48, '_wp_attached_file', '2020/04/g3.png'),
(78, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g3-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(79, 49, '_wp_attached_file', '2020/04/g4.png'),
(80, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g4.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g4-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(81, 50, '_wp_attached_file', '2020/04/g5.png'),
(82, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g5.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g5-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(83, 51, '_wp_attached_file', '2020/04/g6.png'),
(84, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g6.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g6-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(85, 52, '_wp_attached_file', '2020/04/g7.png'),
(86, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:120;s:4:\"file\";s:14:\"2020/04/g7.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g7-150x120.png\";s:5:\"width\";i:150;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(87, 44, '_thumbnail_id', '45'),
(88, 53, '_edit_last', '1'),
(89, 53, '_thumbnail_id', '52'),
(90, 53, '_edit_lock', '1587134244:1'),
(91, 54, '_edit_last', '1'),
(92, 54, '_thumbnail_id', '48'),
(93, 54, '_edit_lock', '1587134261:1'),
(94, 55, '_edit_last', '1'),
(95, 55, '_thumbnail_id', '49'),
(96, 55, '_edit_lock', '1587141591:1'),
(97, 56, '_edit_last', '1'),
(98, 56, '_thumbnail_id', '50'),
(99, 56, '_edit_lock', '1587134301:1'),
(100, 57, '_edit_last', '1'),
(101, 57, '_thumbnail_id', '51'),
(102, 57, '_edit_lock', '1587134311:1'),
(103, 58, '_edit_last', '1'),
(104, 58, '_thumbnail_id', '47'),
(105, 58, '_edit_lock', '1587134348:1'),
(106, 59, '_edit_last', '1'),
(107, 59, '_thumbnail_id', '46'),
(108, 59, '_edit_lock', '1587134385:1'),
(109, 60, '_edit_last', '1'),
(110, 60, '_edit_lock', '1587146710:1'),
(111, 60, '_thumbnail_id', '49'),
(112, 61, '_edit_last', '1'),
(113, 61, '_edit_lock', '1587146729:1'),
(114, 61, '_thumbnail_id', '50'),
(115, 62, '_edit_last', '1'),
(116, 62, '_edit_lock', '1587146944:1'),
(117, 62, '_thumbnail_id', '46'),
(118, 63, '_edit_last', '1'),
(119, 63, '_edit_lock', '1587146958:1'),
(120, 63, '_thumbnail_id', '52'),
(121, 64, '_edit_last', '1'),
(122, 64, '_edit_lock', '1587146971:1'),
(123, 64, '_thumbnail_id', '47'),
(124, 65, '_edit_last', '1'),
(125, 65, '_edit_lock', '1587146985:1'),
(126, 65, '_thumbnail_id', '51'),
(127, 66, '_edit_last', '1'),
(128, 66, '_edit_lock', '1587147002:1'),
(129, 66, '_thumbnail_id', '50'),
(130, 67, '_edit_last', '1'),
(131, 67, '_edit_lock', '1587147016:1'),
(132, 67, '_thumbnail_id', '51'),
(133, 68, '_edit_last', '1'),
(134, 68, '_edit_lock', '1587147034:1'),
(135, 68, '_thumbnail_id', '45'),
(136, 69, '_edit_last', '1'),
(137, 69, '_edit_lock', '1598812315:1'),
(138, 70, '_wp_attached_file', '2020/04/piscina.png'),
(139, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:670;s:6:\"height\";i:480;s:4:\"file\";s:19:\"2020/04/piscina.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"piscina-300x215.png\";s:5:\"width\";i:300;s:6:\"height\";i:215;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"piscina-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(140, 69, '_thumbnail_id', '70'),
(141, 71, '_form', '[text* modo-contato-pop-up id:modo-contato-pop-up placeholder \"Modo contato\"]<label for=\"input-nome-pop-up\">[text* input-nome-pop-up id:input-nome-pop-up placeholder \"Nome\"]<span class=\"span-label-input\">Nome</span></label><label for=\"input-telefone-pop-up\">[tel* input-telefone-pop-up id:input-telefone-pop-up placeholder \"DDD + Telefone\"]<span class=\"span-label-input\">Telefone</span></label><label for=\"input-email-pop-up\">[email* input-email-pop-up id:input-email-pop-up placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">E-mail</span></label><label for=\"input-confirmar-email-pop-up\">[email* input-confirmar-email-pop-up id:input-confirmar-email-pop-up placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">Confirmar E-mail</span></label><div class=\"div-button-interesse\">[submit class:button-interesse \"Tenho interesse\"]</div>'),
(142, 71, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:32:\"Terrazzo \"Quero uma simulação\"\";s:6:\"sender\";s:22:\"Terrazzo <dev@gran.ag>\";s:9:\"recipient\";s:11:\"dev@gran.ag\";s:4:\"body\";s:292:\"De: [input-nome-pop-up]\nTelefone: [input-telefone-pop-up]\n\nEmail: [input-email-pop-up]\nConfirmação de email: [input-confirmar-email-pop-up]\n\nComo prefere ser contatado: [modo-contato-pop-up]\n\n-- \nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(143, 71, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"Terrazzo \"[your-subject]\"\";s:6:\"sender\";s:22:\"Terrazzo <dev@gran.ag>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:128:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\";s:18:\"additional_headers\";s:21:\"Reply-To: dev@gran.ag\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(144, 71, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(145, 71, '_additional_settings', ''),
(146, 71, '_locale', 'pt_BR'),
(149, 72, '_form', '[text* modo-contato id:modo-contato placeholder \"Modo contato\"]<label for=\"input-nome\">[text* input-nome id:input-nome placeholder \"Nome\"]<span class=\"span-label-input\">Nome</span></label><label for=\"input-telefone\">[tel* input-telefone id:input-telefone placeholder \"DDD + Telefone\"]<span class=\"span-label-input\">Telefone</span></label><label for=\"input-email\">[email* input-email id:input-email placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">E-mail</span></label><label for=\"input-confirmar-email\">[email* input-confirmar-email id:input-confirmar-email placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">Confirmar E-mail</span></label><div class=\"div-button-interesse\">[submit class:button-interesse \"Tenho interesse\"]</div>'),
(150, 72, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:32:\"Terrazzo \"Quero uma simulação\"\";s:6:\"sender\";s:22:\"Terrazzo <dev@gran.ag>\";s:9:\"recipient\";s:11:\"dev@gran.ag\";s:4:\"body\";s:257:\"De: [input-nome]\nTelefone: [input-telefone]\n\nEmail: [input-email]\nConfirmação de email: [input-confirmar-email]\n\nComo prefere ser contatado: [modo-contato]\n\n-- \nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(151, 72, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"Terrazzo \"[your-subject]\"\";s:6:\"sender\";s:22:\"Terrazzo <dev@gran.ag>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:128:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\";s:18:\"additional_headers\";s:21:\"Reply-To: dev@gran.ag\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(152, 72, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(153, 72, '_additional_settings', ''),
(154, 72, '_locale', 'pt_BR'),
(157, 73, '_wp_attached_file', '2020/04/ATR-Valparaiso.mp4'),
(158, 73, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:302809746;s:9:\"mime_type\";s:9:\"video/mp4\";s:6:\"length\";i:122;s:16:\"length_formatted\";s:4:\"2:02\";s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:7:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";}s:17:\"created_timestamp\";i:1571670711;}'),
(161, 76, '_edit_last', '1'),
(162, 76, '_edit_lock', '1588598938:1'),
(163, 77, '_wp_attached_file', '2020/04/terrazzo.jpg'),
(164, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:445;s:6:\"height\";i:642;s:4:\"file\";s:20:\"2020/04/terrazzo.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"terrazzo-208x300.jpg\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"terrazzo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(165, 78, '_wp_attached_file', '2020/04/terrazzo1.jpg'),
(166, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:445;s:6:\"height\";i:642;s:4:\"file\";s:21:\"2020/04/terrazzo1.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"terrazzo1-208x300.jpg\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"terrazzo1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(167, 76, '_thumbnail_id', '77'),
(209, 87, '_edit_last', '1'),
(210, 87, '_edit_lock', '1588597976:1'),
(211, 88, '_wp_attached_file', '2020/05/predio.jpg'),
(212, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:449;s:6:\"height\";i:648;s:4:\"file\";s:18:\"2020/05/predio.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"predio-208x300.jpg\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"predio-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 89, '_wp_attached_file', '2020/05/predio1.jpg'),
(214, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:449;s:6:\"height\";i:648;s:4:\"file\";s:19:\"2020/05/predio1.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"predio1-208x300.jpg\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"predio1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(215, 90, '_wp_attached_file', '2020/05/predio2.jpg'),
(216, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:449;s:6:\"height\";i:648;s:4:\"file\";s:19:\"2020/05/predio2.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"predio2-208x300.jpg\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"predio2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 91, '_wp_attached_file', '2020/05/predio3.jpg'),
(218, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:449;s:6:\"height\";i:648;s:4:\"file\";s:19:\"2020/05/predio3.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"predio3-208x300.jpg\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"predio3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(219, 87, '_thumbnail_id', '89'),
(220, 92, '_edit_last', '1'),
(221, 92, '_edit_lock', '1588597976:1'),
(222, 92, '_thumbnail_id', '91'),
(223, 93, '_edit_last', '1'),
(224, 93, '_edit_lock', '1588597976:1'),
(225, 93, '_thumbnail_id', '90'),
(226, 94, '_edit_last', '1'),
(227, 94, '_edit_lock', '1588597975:1'),
(228, 94, '_thumbnail_id', '88'),
(229, 76, '_wp_old_slug', 'terrazzo__trashed'),
(230, 96, '_wp_attached_file', '2020/08/planta1.png'),
(231, 96, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:853;s:6:\"height\";i:464;s:4:\"file\";s:19:\"2020/08/planta1.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"planta1-300x163.png\";s:5:\"width\";i:300;s:6:\"height\";i:163;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"planta1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"planta1-768x418.png\";s:5:\"width\";i:768;s:6:\"height\";i:418;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(232, 29, 'terrazzo_imagem_planta', '96'),
(233, 29, 'terrazzo_imagem_3d_planta', '20'),
(234, 39, 'terrazzo_imagem_planta', '96'),
(235, 39, 'terrazzo_imagem_3d_planta', '20');

-- --------------------------------------------------------

--
-- Table structure for table `tr_posts`
--

CREATE TABLE `tr_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_posts`
--

INSERT INTO `tr_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-04-08 17:29:32', '2020-04-08 20:29:32', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2020-04-08 17:29:32', '2020-04-08 20:29:32', '', 0, 'http://localhost/projetos/terrazzo_site/?p=1', 0, 'post', '', 1),
(7, 1, '2020-04-08 18:26:48', '2020-04-08 21:26:48', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2020-04-08 18:26:48', '2020-04-08 21:26:48', '', 0, 'http://localhost/projetos/terrazzo_site/?page_id=7', 0, 'page', '', 0),
(8, 1, '2020-04-08 18:26:48', '2020-04-08 21:26:48', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2020-04-08 18:26:48', '2020-04-08 21:26:48', '', 7, 'http://localhost/projetos/terrazzo_site/index.php/2020/04/08/7-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2020-04-09 14:20:53', '2020-04-09 17:20:53', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2020-04-09 14:20:53', '2020-04-09 17:20:53', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/logo.png', 0, 'attachment', 'image/png', 0),
(12, 1, '2020-04-09 14:21:59', '2020-04-09 17:21:59', '', 'email', '', 'inherit', 'open', 'closed', '', 'email', '', '', '2020-04-09 14:21:59', '2020-04-09 17:21:59', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/email.svg', 0, 'attachment', 'image/svg+xml', 0),
(13, 1, '2020-04-09 14:22:00', '2020-04-09 17:22:00', '', 'telefone', '', 'inherit', 'open', 'closed', '', 'telefone', '', '', '2020-04-09 14:22:00', '2020-04-09 17:22:00', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/telefone.svg', 0, 'attachment', 'image/svg+xml', 0),
(14, 1, '2020-04-09 14:22:00', '2020-04-09 17:22:00', '', 'videocall', '', 'inherit', 'open', 'closed', '', 'videocall', '', '', '2020-04-09 14:22:00', '2020-04-09 17:22:00', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/videocall.svg', 0, 'attachment', 'image/svg+xml', 0),
(15, 1, '2020-04-09 14:22:01', '2020-04-09 17:22:01', '', 'zap', '', 'inherit', 'open', 'closed', '', 'zap', '', '', '2020-04-09 14:22:01', '2020-04-09 17:22:01', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/zap.svg', 0, 'attachment', 'image/svg+xml', 0),
(16, 1, '2020-04-09 14:30:17', '2020-04-09 17:30:17', '', 'celular', '', 'inherit', 'open', 'closed', '', 'celular', '', '', '2020-04-09 14:30:17', '2020-04-09 17:30:17', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/celular.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2020-04-09 14:30:41', '2020-04-09 17:30:41', '', 'appstore', '', 'inherit', 'open', 'closed', '', 'appstore', '', '', '2020-04-09 14:30:41', '2020-04-09 17:30:41', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/appstore.png', 0, 'attachment', 'image/png', 0),
(18, 1, '2020-04-09 14:30:42', '2020-04-09 17:30:42', '', 'playstore', '', 'inherit', 'open', 'closed', '', 'playstore', '', '', '2020-04-09 14:30:42', '2020-04-09 17:30:42', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/playstore.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2020-04-09 14:31:52', '2020-04-09 17:31:52', '', 'eric', '', 'inherit', 'open', 'closed', '', 'eric', '', '', '2020-04-09 14:31:52', '2020-04-09 17:31:52', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/eric.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2020-04-09 14:44:43', '2020-04-09 17:44:43', '', 'video1', '', 'inherit', 'open', 'closed', '', 'video1', '', '', '2020-04-09 14:44:43', '2020-04-09 17:44:43', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/video1.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2020-04-14 15:57:35', '2020-04-14 18:57:35', '', 'Planta 01', '', 'publish', 'closed', 'closed', '', 'planta-01', '', '', '2020-08-30 12:52:19', '2020-08-30 15:52:19', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=planta&#038;p=29', 0, 'planta', '', 0),
(30, 1, '2020-04-14 15:53:10', '2020-04-14 18:53:10', '', 'cama', '', 'inherit', 'open', 'closed', '', 'cama', '', '', '2020-04-14 15:53:10', '2020-04-14 18:53:10', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/cama.svg', 0, 'attachment', 'image/svg+xml', 0),
(31, 1, '2020-04-14 15:53:10', '2020-04-14 18:53:10', '', 'carro', '', 'inherit', 'open', 'closed', '', 'carro', '', '', '2020-04-14 15:53:10', '2020-04-14 18:53:10', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/carro.svg', 0, 'attachment', 'image/svg+xml', 0),
(32, 1, '2020-04-14 15:53:11', '2020-04-14 18:53:11', '', 'fingerprint', '', 'inherit', 'open', 'closed', '', 'fingerprint', '', '', '2020-04-14 15:53:11', '2020-04-14 18:53:11', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/fingerprint.svg', 0, 'attachment', 'image/svg+xml', 0),
(33, 1, '2020-04-14 15:53:11', '2020-04-14 18:53:11', '', 'gas', '', 'inherit', 'open', 'closed', '', 'gas', '', '', '2020-04-14 15:53:11', '2020-04-14 18:53:11', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/gas.svg', 0, 'attachment', 'image/svg+xml', 0),
(34, 1, '2020-04-14 15:53:12', '2020-04-14 18:53:12', '', 'metragem', '', 'inherit', 'open', 'closed', '', 'metragem', '', '', '2020-04-14 15:53:12', '2020-04-14 18:53:12', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/metragem.svg', 0, 'attachment', 'image/svg+xml', 0),
(35, 1, '2020-04-14 15:53:12', '2020-04-14 18:53:12', '', 'mute', '', 'inherit', 'open', 'closed', '', 'mute', '', '', '2020-04-14 15:53:12', '2020-04-14 18:53:12', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/mute.svg', 0, 'attachment', 'image/svg+xml', 0),
(36, 1, '2020-04-14 15:53:13', '2020-04-14 18:53:13', '', 'underfloor', '', 'inherit', 'open', 'closed', '', 'underfloor', '', '', '2020-04-14 15:53:13', '2020-04-14 18:53:13', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/underfloor.svg', 0, 'attachment', 'image/svg+xml', 0),
(37, 1, '2020-04-14 15:56:35', '2020-04-14 18:56:35', '', 'planta', '', 'inherit', 'open', 'closed', '', 'planta', '', '', '2020-04-14 15:56:35', '2020-04-14 18:56:35', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/planta.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2020-04-14 16:01:47', '2020-04-14 19:01:47', '', 'Planta 02', '', 'publish', 'closed', 'closed', '', 'planta-02', '', '', '2020-08-30 12:52:27', '2020-08-30 15:52:27', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=planta&#038;p=39', 0, 'planta', '', 0),
(42, 1, '2020-04-15 12:48:06', '2020-04-15 15:48:06', '', 'bannervideo', '', 'inherit', 'open', 'closed', '', 'bannervideo', '', '', '2020-04-15 12:48:06', '2020-04-15 15:48:06', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/bannervideo.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2020-04-17 11:39:25', '2020-04-17 14:39:25', '', 'Sala de estar', '', 'publish', 'closed', 'closed', '', 'sala-de-estar', '', '', '2020-04-17 11:39:25', '2020-04-17 14:39:25', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=44', 0, 'galeria', '', 0),
(45, 1, '2020-04-17 11:39:09', '2020-04-17 14:39:09', '', 'g', '', 'inherit', 'open', 'closed', '', 'g', '', '', '2020-04-17 11:39:09', '2020-04-17 14:39:09', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2020-04-17 11:39:11', '2020-04-17 14:39:11', '', 'g1', '', 'inherit', 'open', 'closed', '', 'g1', '', '', '2020-04-17 11:39:11', '2020-04-17 14:39:11', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g1.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2020-04-17 11:39:12', '2020-04-17 14:39:12', '', 'g2', '', 'inherit', 'open', 'closed', '', 'g2', '', '', '2020-04-17 11:39:12', '2020-04-17 14:39:12', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g2.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2020-04-17 11:39:13', '2020-04-17 14:39:13', '', 'g3', '', 'inherit', 'open', 'closed', '', 'g3', '', '', '2020-04-17 11:39:13', '2020-04-17 14:39:13', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g3.png', 0, 'attachment', 'image/png', 0),
(49, 1, '2020-04-17 11:39:14', '2020-04-17 14:39:14', '', 'g4', '', 'inherit', 'open', 'closed', '', 'g4', '', '', '2020-04-17 11:39:14', '2020-04-17 14:39:14', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g4.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2020-04-17 11:39:15', '2020-04-17 14:39:15', '', 'g5', '', 'inherit', 'open', 'closed', '', 'g5', '', '', '2020-04-17 11:39:15', '2020-04-17 14:39:15', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g5.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2020-04-17 11:39:16', '2020-04-17 14:39:16', '', 'g6', '', 'inherit', 'open', 'closed', '', 'g6', '', '', '2020-04-17 11:39:16', '2020-04-17 14:39:16', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g6.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2020-04-17 11:39:17', '2020-04-17 14:39:17', '', 'g7', '', 'inherit', 'open', 'closed', '', 'g7', '', '', '2020-04-17 11:39:17', '2020-04-17 14:39:17', '', 44, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/g7.png', 0, 'attachment', 'image/png', 0),
(53, 1, '2020-04-17 11:39:45', '2020-04-17 14:39:45', '', 'Sala de jantar', '', 'publish', 'closed', 'closed', '', 'sala-de-jantar', '', '', '2020-04-17 11:39:45', '2020-04-17 14:39:45', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=53', 7, 'galeria', '', 0),
(54, 1, '2020-04-17 11:40:03', '2020-04-17 14:40:03', '', 'Quarto', '', 'publish', 'closed', 'closed', '', 'quarto', '', '', '2020-04-17 11:40:03', '2020-04-17 14:40:03', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=54', 6, 'galeria', '', 0),
(55, 1, '2020-04-17 11:40:21', '2020-04-17 14:40:21', '', 'Sala de estar', '', 'publish', 'closed', 'closed', '', 'sala-de-estar-2', '', '', '2020-04-17 11:40:21', '2020-04-17 14:40:21', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=55', 5, 'galeria', '', 0),
(56, 1, '2020-04-17 11:40:43', '2020-04-17 14:40:43', '', 'Varanda', '', 'publish', 'closed', 'closed', '', 'varanda', '', '', '2020-04-17 11:40:43', '2020-04-17 14:40:43', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=56', 4, 'galeria', '', 0),
(57, 1, '2020-04-17 11:40:53', '2020-04-17 14:40:53', '', 'Sala', '', 'publish', 'closed', 'closed', '', 'sala', '', '', '2020-04-17 11:40:53', '2020-04-17 14:40:53', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=57', 3, 'galeria', '', 0),
(58, 1, '2020-04-17 11:41:30', '2020-04-17 14:41:30', '', 'Escadas', '', 'publish', 'closed', 'closed', '', 'escadas', '', '', '2020-04-17 11:41:30', '2020-04-17 14:41:30', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=58', 2, 'galeria', '', 0),
(59, 1, '2020-04-17 11:41:56', '2020-04-17 14:41:56', '', 'Cozinha', '', 'publish', 'closed', 'closed', '', 'cozinha', '', '', '2020-04-17 11:41:56', '2020-04-17 14:41:56', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=galeria&#038;p=59', 1, 'galeria', '', 0),
(60, 1, '2020-04-17 15:07:31', '2020-04-17 18:07:31', '', 'Salão de jogos', '', 'publish', 'closed', 'closed', '', 'salao-de-jogos', '', '', '2020-04-17 15:07:31', '2020-04-17 18:07:31', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=60', 0, 'diferencial', '', 0),
(61, 1, '2020-04-17 15:07:48', '2020-04-17 18:07:48', '', 'Parquinho', '', 'publish', 'closed', 'closed', '', 'parquinho', '', '', '2020-04-17 15:07:48', '2020-04-17 18:07:48', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=61', 0, 'diferencial', '', 0),
(62, 1, '2020-04-17 15:11:27', '2020-04-17 18:11:27', '', 'Sauna', '', 'publish', 'closed', 'closed', '', 'sauna', '', '', '2020-04-17 15:11:27', '2020-04-17 18:11:27', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=62', 0, 'diferencial', '', 0),
(63, 1, '2020-04-17 15:11:41', '2020-04-17 18:11:41', '', 'Salão de eventos', '', 'publish', 'closed', 'closed', '', 'salao-de-eventos', '', '', '2020-04-17 15:11:41', '2020-04-17 18:11:41', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=63', 0, 'diferencial', '', 0),
(64, 1, '2020-04-17 15:11:51', '2020-04-17 18:11:51', '', 'Academia', '', 'publish', 'closed', 'closed', '', 'academia', '', '', '2020-04-17 15:11:51', '2020-04-17 18:11:51', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=64', 0, 'diferencial', '', 0),
(65, 1, '2020-04-17 15:12:06', '2020-04-17 18:12:06', '', 'Brinquedoteca', '', 'publish', 'closed', 'closed', '', 'brinquedoteca', '', '', '2020-04-17 15:12:06', '2020-04-17 18:12:06', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=65', 0, 'diferencial', '', 0),
(66, 1, '2020-04-17 15:12:22', '2020-04-17 18:12:22', '', 'Quadra poliesportiva', '', 'publish', 'closed', 'closed', '', 'quadra-poliesportiva', '', '', '2020-04-17 15:12:22', '2020-04-17 18:12:22', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=66', 0, 'diferencial', '', 0),
(67, 1, '2020-04-17 15:12:37', '2020-04-17 18:12:37', '', 'Salão gourmet', '', 'publish', 'closed', 'closed', '', 'salao-gourmet', '', '', '2020-04-17 15:12:37', '2020-04-17 18:12:37', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=67', 0, 'diferencial', '', 0),
(68, 1, '2020-04-17 15:12:55', '2020-04-17 18:12:55', '', 'Pista de skate', '', 'publish', 'closed', 'closed', '', 'pista-de-skate', '', '', '2020-04-17 15:12:55', '2020-04-17 18:12:55', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=68', 0, 'diferencial', '', 0),
(69, 1, '2020-04-17 15:13:34', '2020-04-17 18:13:34', '', 'Piscina com borda infinita', '', 'publish', 'closed', 'closed', '', 'piscina-com-borda-infinita', '', '', '2020-04-17 15:13:34', '2020-04-17 18:13:34', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=diferencial&#038;p=69', 0, 'diferencial', '', 0),
(70, 1, '2020-04-17 15:13:32', '2020-04-17 18:13:32', '', 'piscina', '', 'inherit', 'open', 'closed', '', 'piscina', '', '', '2020-04-17 15:13:32', '2020-04-17 18:13:32', '', 69, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/piscina.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2020-04-17 16:42:51', '2020-04-17 19:42:51', '[text* modo-contato-pop-up id:modo-contato-pop-up placeholder \"Modo contato\"]<label for=\"input-nome-pop-up\">[text* input-nome-pop-up id:input-nome-pop-up placeholder \"Nome\"]<span class=\"span-label-input\">Nome</span></label><label for=\"input-telefone-pop-up\">[tel* input-telefone-pop-up id:input-telefone-pop-up placeholder \"DDD + Telefone\"]<span class=\"span-label-input\">Telefone</span></label><label for=\"input-email-pop-up\">[email* input-email-pop-up id:input-email-pop-up placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">E-mail</span></label><label for=\"input-confirmar-email-pop-up\">[email* input-confirmar-email-pop-up id:input-confirmar-email-pop-up placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">Confirmar E-mail</span></label><div class=\"div-button-interesse\">[submit class:button-interesse \"Tenho interesse\"]</div>\n1\nTerrazzo \"Quero uma simulação\"\nTerrazzo <dev@gran.ag>\ndev@gran.ag\nDe: [input-nome-pop-up]\r\nTelefone: [input-telefone-pop-up]\r\n\r\nEmail: [input-email-pop-up]\r\nConfirmação de email: [input-confirmar-email-pop-up]\r\n\r\nComo prefere ser contatado: [modo-contato-pop-up]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\n\n\n\n\n\nTerrazzo \"[your-subject]\"\nTerrazzo <dev@gran.ag>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\nReply-To: dev@gran.ag\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Quero uma simulação', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2020-04-17 16:55:30', '2020-04-17 19:55:30', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=wpcf7_contact_form&#038;p=71', 0, 'wpcf7_contact_form', '', 0),
(72, 1, '2020-04-17 16:59:32', '2020-04-17 19:59:32', '[text* modo-contato id:modo-contato placeholder \"Modo contato\"]<label for=\"input-nome\">[text* input-nome id:input-nome placeholder \"Nome\"]<span class=\"span-label-input\">Nome</span></label><label for=\"input-telefone\">[tel* input-telefone id:input-telefone placeholder \"DDD + Telefone\"]<span class=\"span-label-input\">Telefone</span></label><label for=\"input-email\">[email* input-email id:input-email placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">E-mail</span></label><label for=\"input-confirmar-email\">[email* input-confirmar-email id:input-confirmar-email placeholder \"exemplo@email.com.br\"]<span class=\"span-label-input\">Confirmar E-mail</span></label><div class=\"div-button-interesse\">[submit class:button-interesse \"Tenho interesse\"]</div>\n1\nTerrazzo \"Quero uma simulação\"\nTerrazzo <dev@gran.ag>\ndev@gran.ag\nDe: [input-nome]\r\nTelefone: [input-telefone]\r\n\r\nEmail: [input-email]\r\nConfirmação de email: [input-confirmar-email]\r\n\r\nComo prefere ser contatado: [modo-contato]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\n\n\n\n\n\nTerrazzo \"[your-subject]\"\nTerrazzo <dev@gran.ag>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Terrazzo (http://localhost/projetos/terrazzo_site)\nReply-To: dev@gran.ag\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Deixe seu contato', '', 'publish', 'closed', 'closed', '', 'deixe-seu-contato', '', '', '2020-04-22 10:54:32', '2020-04-22 13:54:32', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=wpcf7_contact_form&#038;p=72', 0, 'wpcf7_contact_form', '', 0),
(73, 1, '2020-04-17 17:25:51', '2020-04-17 20:25:51', '', 'ATR Valparaiso', '', 'inherit', 'open', 'closed', '', 'atr-valparaiso', '', '', '2020-04-17 17:25:51', '2020-04-17 20:25:51', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/ATR-Valparaiso.mp4', 0, 'attachment', 'video/mp4', 0),
(76, 1, '2020-04-24 14:48:31', '2020-04-24 17:48:31', '', 'Terrazzo', '', 'publish', 'closed', 'closed', '', 'terrazzo', '', '', '2020-05-04 10:31:06', '2020-05-04 13:31:06', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=sobre&#038;p=76', 0, 'sobre', '', 0),
(77, 1, '2020-04-24 14:48:26', '2020-04-24 17:48:26', '', 'terrazzo', '', 'inherit', 'open', 'closed', '', 'terrazzo', '', '', '2020-04-24 14:48:26', '2020-04-24 17:48:26', '', 76, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/terrazzo.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2020-04-24 14:48:27', '2020-04-24 17:48:27', '', 'terrazzo1', '', 'inherit', 'open', 'closed', '', 'terrazzo1', '', '', '2020-04-24 14:48:27', '2020-04-24 17:48:27', '', 76, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/04/terrazzo1.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2020-05-04 10:13:44', '2020-05-04 13:13:44', '', 'Terrazzo frente', '', 'publish', 'closed', 'closed', '', 'terrazzo-frente', '', '', '2020-05-04 10:13:44', '2020-05-04 13:13:44', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=sobre&#038;p=87', 1, 'sobre', '', 0),
(88, 1, '2020-05-04 10:13:30', '2020-05-04 13:13:30', '', 'predio', '', 'inherit', 'open', 'closed', '', 'predio', '', '', '2020-05-04 10:13:30', '2020-05-04 13:13:30', '', 87, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/05/predio.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2020-05-04 10:13:31', '2020-05-04 13:13:31', '', 'predio1', '', 'inherit', 'open', 'closed', '', 'predio1', '', '', '2020-05-04 10:13:31', '2020-05-04 13:13:31', '', 87, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/05/predio1.jpg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2020-05-04 10:13:33', '2020-05-04 13:13:33', '', 'predio2', '', 'inherit', 'open', 'closed', '', 'predio2', '', '', '2020-05-04 10:13:33', '2020-05-04 13:13:33', '', 87, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/05/predio2.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2020-05-04 10:13:34', '2020-05-04 13:13:34', '', 'predio3', '', 'inherit', 'open', 'closed', '', 'predio3', '', '', '2020-05-04 10:13:34', '2020-05-04 13:13:34', '', 87, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/05/predio3.jpg', 0, 'attachment', 'image/jpeg', 0),
(92, 1, '2020-05-04 10:14:04', '2020-05-04 13:14:04', '', 'Terrazzo lateral', '', 'publish', 'closed', 'closed', '', 'terrazzo-lateral', '', '', '2020-05-04 10:14:04', '2020-05-04 13:14:04', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=sobre&#038;p=92', 4, 'sobre', '', 0),
(93, 1, '2020-05-04 10:14:25', '2020-05-04 13:14:25', '', 'Terrazzo trás', '', 'publish', 'closed', 'closed', '', 'terrazzo-tras', '', '', '2020-05-04 10:14:25', '2020-05-04 13:14:25', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=sobre&#038;p=93', 3, 'sobre', '', 0),
(94, 1, '2020-05-04 10:15:04', '2020-05-04 13:15:04', '', 'Terrazzo lateral', '', 'publish', 'closed', 'closed', '', 'terrazzo-lateral-2', '', '', '2020-05-04 10:15:04', '2020-05-04 13:15:04', '', 0, 'http://localhost/projetos/terrazzo_site/?post_type=sobre&#038;p=94', 2, 'sobre', '', 0),
(95, 1, '2020-08-26 22:23:24', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-08-26 22:23:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/terrazzo_site/?p=95', 0, 'post', '', 0),
(96, 1, '2020-08-30 12:52:08', '2020-08-30 15:52:08', '', 'planta1', '', 'inherit', 'open', 'closed', '', 'planta1', '', '', '2020-08-30 12:52:08', '2020-08-30 15:52:08', '', 0, 'http://localhost/projetos/terrazzo_site/wp-content/uploads/2020/08/planta1.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_termmeta`
--

CREATE TABLE `tr_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tr_terms`
--

CREATE TABLE `tr_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_terms`
--

INSERT INTO `tr_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_term_relationships`
--

CREATE TABLE `tr_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_term_relationships`
--

INSERT INTO `tr_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_term_taxonomy`
--

CREATE TABLE `tr_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_term_taxonomy`
--

INSERT INTO `tr_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tr_usermeta`
--

CREATE TABLE `tr_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_usermeta`
--

INSERT INTO `tr_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'terrazzo'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'tr_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'tr_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'tr_dashboard_quick_press_last_post_id', '95'),
(19, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(20, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:21:\"dashboard_site_health\";i:1;s:19:\"dashboard_right_now\";i:2;s:18:\"dashboard_activity\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(22, 1, 'tr_r_tru_u_x', 'a:2:{s:2:\"id\";s:0:\"\";s:7:\"expires\";i:86400;}'),
(23, 1, 'tr_user-settings', 'libraryContent=browse'),
(24, 1, 'tr_user-settings-time', '1586453108'),
(27, 1, 'session_tokens', 'a:1:{s:64:\"3b2fd91d5f3927f647d43c34812dc9e03802e412ad9a5231876ec1d542a09fd4\";a:4:{s:10:\"expiration\";i:1598969122;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36\";s:5:\"login\";i:1598796322;}}');

-- --------------------------------------------------------

--
-- Table structure for table `tr_users`
--

CREATE TABLE `tr_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `tr_users`
--

INSERT INTO `tr_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'terrazzo', '$P$BqBk7fivoDmj79pccWtPur0TnKlDlp.', 'terrazzo', 'dev@gran.ag', 'http://localhost/projetos/terrazzo_site', '2020-04-08 20:29:27', '', 0, 'terrazzo');

-- --------------------------------------------------------

--
-- Table structure for table `tr_yoast_seo_links`
--

CREATE TABLE `tr_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tr_yoast_seo_meta`
--

CREATE TABLE `tr_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_commentmeta`
--
ALTER TABLE `tr_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `tr_comments`
--
ALTER TABLE `tr_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `tr_links`
--
ALTER TABLE `tr_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `tr_options`
--
ALTER TABLE `tr_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `tr_postmeta`
--
ALTER TABLE `tr_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `tr_posts`
--
ALTER TABLE `tr_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `tr_termmeta`
--
ALTER TABLE `tr_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `tr_terms`
--
ALTER TABLE `tr_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `tr_term_relationships`
--
ALTER TABLE `tr_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `tr_term_taxonomy`
--
ALTER TABLE `tr_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `tr_usermeta`
--
ALTER TABLE `tr_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `tr_users`
--
ALTER TABLE `tr_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `tr_yoast_seo_links`
--
ALTER TABLE `tr_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `tr_yoast_seo_meta`
--
ALTER TABLE `tr_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_commentmeta`
--
ALTER TABLE `tr_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_comments`
--
ALTER TABLE `tr_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tr_links`
--
ALTER TABLE `tr_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_options`
--
ALTER TABLE `tr_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=796;
--
-- AUTO_INCREMENT for table `tr_postmeta`
--
ALTER TABLE `tr_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT for table `tr_posts`
--
ALTER TABLE `tr_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `tr_termmeta`
--
ALTER TABLE `tr_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_terms`
--
ALTER TABLE `tr_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tr_term_taxonomy`
--
ALTER TABLE `tr_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tr_usermeta`
--
ALTER TABLE `tr_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tr_users`
--
ALTER TABLE `tr_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tr_yoast_seo_links`
--
ALTER TABLE `tr_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
