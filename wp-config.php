<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_terrazzo_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'BEYH)2hgc.]e_6P+#^=xo<.rcJ;MYg#cIMq~ne|V4D)SI1W^3HOuLnp*?9z=0}ql' );
define( 'SECURE_AUTH_KEY',  'epF:3,4IvlDKrk<H[oNWpn5RAI+gP[E0ehP*{`gO*Z HvR6d3r6^?<wsR0[WZNcB' );
define( 'LOGGED_IN_KEY',    '-o bq]~^6x<o{QcO`R_WL?7#S6 0u.ef:nx,[^{ !htnFC._th@w[UM|T1^#KpFz' );
define( 'NONCE_KEY',        '+9#]$!PE]7^(oLQnUB/0no_A02KhAy+m7dvy<$X8L~szGx0x^z>I/ISZWpTh$:rr' );
define( 'AUTH_SALT',        '!(&{y:x*j>=_|iozZ+#c^k[&6v9|5<#5%%Lt,7$N6sT~tPJ~QZWKPhKk?:as?=mH' );
define( 'SECURE_AUTH_SALT', 'u`66[~S :Hn9PM4Ou;a>yyi&!yDiJr/:.z*}?pr+)#9c^h|}$D-$U&:Z:}jNP}ck' );
define( 'LOGGED_IN_SALT',   'AByw/>;2CB2=f/NS6oisSR]-ge+tAW#i{VZ_>5FXABbOH`)ZE,X}-peZ)_Aubs/m' );
define( 'NONCE_SALT',       '#CK3j<~~b<0%-|?OcW~pcQqk!HY|8)NF9)SM1K_;fJ{6`C>fa:7,:+;+fTu#0Rg$' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'tr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

define('FS_METHOD','direct');