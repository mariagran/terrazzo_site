(function(){

	$(document).ready(function(){

		$('.carrossel-planta').owlCarousel({
			items: 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			smartSpeed: 450,
		});

		$('.carrossel-diferenciais-planta').owlCarousel({
			items: 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			smartSpeed: 450,
		});

		let dotsPlantas = document.querySelectorAll('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .owl-dots .owl-dot');
		let dotsDiferenciaisPlantas = document.querySelectorAll('.pg-inicial .secao-plantas .div-info-planta .carrossel-diferenciais-planta .owl-dots .owl-dot');
		function setAttributes(){
			dotsPlantas.forEach(function(item, i){
				item.setAttribute('data-id', i);
			});
			dotsDiferenciaisPlantas.forEach(function(item, i){
				let newId = 'planta'+i;
				item.setAttribute('id', newId);
			});
		}
		setAttributes();

		function changeCarouselItem(item){
			item.addEventListener('click', function(){
				let dataId = item.getAttribute('data-id');
				let dot = document.querySelector('.pg-inicial .secao-plantas .div-info-planta .carrossel-diferenciais-planta .owl-dots #planta'+dataId);

				dot.click();
			});
		}
		dotsPlantas.forEach(changeCarouselItem);

		$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .owl-nav .owl-next').click(function(){
			$('.carrossel-diferenciais-planta .owl-nav .owl-next').click();
		});
		$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .owl-nav .owl-prev').click(function(){
			$('.carrossel-diferenciais-planta .owl-nav .owl-prev').click();
		});

		setTimeout(function(){
			$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .owl-nav .owl-prev').click();
		}, 500);

		$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .owl-nav button').click(function(){
			let dataTitulo = $('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .owl-item.active .item-planta').attr('data-titulo');
			$('.pg-inicial .secao-plantas article .planta-diferenciais .titulo-dois').text(dataTitulo);
		});

		$('.carrossel-diferenciais').owlCarousel({
			items: 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			smartSpeed: 450,
		});

		$('.carrossel-galeria').owlCarousel({
			items: 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,
			smartSpeed: 450,
			URLhashListener:true,
		});

		$('.carrossel-nav-galeria').owlCarousel({
			items: 12,
			dots: false,
			nav: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,
			smartSpeed: 450,
			autoplay: true,
			autoplayTimeout: 2500,
			autoplayHoverPause: true,
			margin: 8,
			responsiveClass:true,			    
			responsive:{
				0:{
					items:3,
				},
				450:{
					items:5,
				},
				640:{
					items:7,
				},
				768:{
					items:8,
				},
				991:{
					items:12,
				},
			}
		});

		$('.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .item-planta .imagem-planta .abrir-menu-planta').click(function(){
			$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .item-planta .sobre-planta').addClass('show-sobre-planta');
		});

		$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .item-planta .sobre-planta .fechar-menu-planta').click(function(){
			$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta .item-planta .sobre-planta').removeClass('show-sobre-planta');
		});

		$('.pg-inicial .secao-diferenciais .diferenciais article ul li a').click(function(){
			$('.pg-inicial .secao-diferenciais .diferenciais article ul li a').removeClass('diferencial-active');

			$(this).addClass('diferencial-active');
		});

		$('.div-button-simulacao .button-simulacao').click(function(e){
			e.preventDefault();
			$('.pg-inicial .div-pop-up-simulacao').addClass('open-pop-up');
			$('body').addClass('stop-scroll');

			setTimeout(function(){
				$('.pg-inicial .div-pop-up-simulacao .container-pop-up-simulacao').addClass('show-pop-up');
			}, 350);
		});

		$('.pg-inicial .div-pop-up-simulacao .container-pop-up-simulacao .pop-up-simulacao .fechar-pop-up-simulacao').click(function(e){
			e.preventDefault();
			$('.pg-inicial .div-pop-up-simulacao .container-pop-up-simulacao').removeClass('show-pop-up');

			setTimeout(function(){
				$('.pg-inicial .div-pop-up-simulacao').removeClass('open-pop-up');
				$('body').removeClass('stop-scroll');
			}, 350);
		});

		$('.pg-inicial .secao-contato .article-contato .modo-contato label input').change(function(){
			let inputValue = $(this).siblings('span').text();

			$('#modo-contato').val(inputValue);
		});

		$('.pg-inicial .div-pop-up-simulacao .container-pop-up-simulacao .pop-up-simulacao .article-contato .modo-contato label input').change(function(){
			let inputValue = $(this).siblings('span').text();

			$('#modo-contato-pop-up').val(inputValue);
		});

		let isPlaying = false;
		document.querySelector('.pg-inicial .secao-destaque .simulacao .video video').onclick = function(){
			if(!isPlaying){
				this.play();
				isPlaying = true;
			} else{
				this.pause();
				isPlaying = false;
			}
		}

		$('.pg-inicial .secao-sobre article .galeria-sobre-terrazzo li figure img').click(function(){
			let imageUrl = $(this).attr('src');

			$('.pg-inicial .secao-sobre .imagem-terrazzo img').attr('src', imageUrl);
		});
		$('.pg-inicial .secao-sobre article .galeria-sobre-terrazzo li:nth-child(1) figure img').click();

		$('.pg-inicial .secao-sobre .button-zoom .maior').click(function(){
			let imageUrl = $('.pg-inicial .secao-sobre .imagem-terrazzo img').attr('src');

			$('.pg-inicial .div-pop-up-imagem figure img').attr('src', imageUrl);

			$('.pg-inicial .div-pop-up-imagem').addClass('abrir-pop-up-imagem');
			setTimeout(function(){
				$('.pg-inicial .div-pop-up-imagem').addClass('show-pop-up-imagem');
				$('body').addClass('stop-scroll');
			}, 350);
		});

		$('.pg-inicial .div-pop-up-imagem .fechar-pop-up-imagem').click(function(){
			$('.pg-inicial .div-pop-up-imagem').removeClass('show-pop-up-imagem');
			$('body').removeClass('stop-scroll');
			setTimeout(function(){
				$('.pg-inicial .div-pop-up-imagem').removeClass('abrir-pop-up-imagem');
			}, 350);
		});

		$('.pop-up-plantas .div-plantas .carrossel-planta .item-planta .imagem-planta .button-zoom .menor img').click(function(){
			$('.pop-up-plantas').removeClass('open-pop-up-plantas');
		});

		$('.pg-inicial .secao-diferenciais .diferenciais .diferencial-terrazzo .menu-diferenciais li').click(function(){
			let dataId = $(this).attr('data-id');
			$('.pg-inicial .secao-diferenciais .diferenciais .diferencial-terrazzo .diferencial').removeClass('active');

			$('.pg-inicial .secao-diferenciais .diferenciais .diferencial-terrazzo .menu-diferenciais li').removeClass('active');
			$(this).addClass('active');

			$('.pg-inicial .secao-diferenciais .diferenciais .diferencial-terrazzo .diferencial#diferencial'+dataId).addClass('active');

		});


		$('.pg-inicial .secao-plantas .div-plantas .menu-plantas li').click(function(){
			let dataId = $(this).attr('data-id');
			console.log(dataId);
			$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta').removeClass('active');

			$('.pg-inicial .secao-plantas .div-plantas .menu-plantas li').removeClass('active');
			$(this).addClass('active');

			$('.pg-inicial .secao-plantas .div-plantas .carrossel-planta.open'+dataId).addClass('active');

		});


		$('#carrosselPlantas .button-zoom').click(function(e){
			let dataId = $(this).attr('data-id');
		
			$('.pop-up-plantas.open'+dataId).addClass('open-pop-up-plantas');

		});

	});

}());