<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
    /*********************************************
                //FUNÇÕES//
    **********************************************/
        if ( ! class_exists( 'Redux' ) ) {
            return;
        }


        // This is your option name where all the Redux data is stored.
        $opt_name = "configuracao";

        // This line is only for altering the demo. Can be easily removed.
        //$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

        /*
         *
         * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
         *
         */

        $sampleHTML = '';
        if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
            Redux_Functions::initWpFilesystem();

            global $wp_filesystem;

            $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
        }

        // Background Patterns Reader
        $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
        $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
        $sample_patterns      = array();

        if ( is_dir( $sample_patterns_path ) ) {

            if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
                $sample_patterns = array();

                while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                    if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                        $name              = explode( '.', $sample_patterns_file );
                        $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                        $sample_patterns[] = array(
                            'alt' => $name,
                            'img' => $sample_patterns_url . $sample_patterns_file
                        );
                    }
                }
            }
        }

        $theme = wp_get_theme(); // For use with some settings. Not necessary.

        $args = array(
            // TYPICAL -> Change these values as you need/desire
            'opt_name'             => $opt_name,
            // This is where your data is stored in the database and also becomes your global variable name.
            'display_name'         => $theme->get( 'Name' ),
            // Name that appears at the top of your panel
            'display_version'      => $theme->get( 'Version' ),
            // Version that appears at the top of your panel
            'menu_type'            => 'menu',
            //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
            'allow_sub_menu'       => true,
            // Show the sections below the admin menu item or not
            'menu_title'           => __( 'Editar informações do site', 'redux-framework-demo' ),
            'page_title'           => __( 'Editar informações do site', 'redux-framework-demo' ),
            // You will need to generate a Google API key to use this feature.
            // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
            'google_api_key'       => '',
            // Set it you want google fonts to update weekly. A google_api_key value is required.
            'google_update_weekly' => false,
            // Must be defined to add google fonts to the typography module
            'async_typography'     => true,
            // Use a asynchronous font on the front end or font string
            //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
            'admin_bar'            => true,
            // Show the panel pages on the admin bar
            'admin_bar_icon'       => 'dashicons-portfolio',
            // Choose an icon for the admin bar menu
            'admin_bar_priority'   => 50,
            // Choose an priority for the admin bar menu
            'global_variable'      => '',
            // Set a different name for your global variable other than the opt_name
            'dev_mode'             => false,
            // Show the time the page took to load, etc
            'update_notice'        => true,
            // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
            'customizer'           => true,
            // Enable basic customizer support
            //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
            //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

            // OPTIONAL -> Give you extra features
            'page_priority'        => null,
            // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
            'page_parent'          => 'themes.php',
            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
            'page_permissions'     => 'manage_options',
            // Permissions needed to access the options panel.
            'menu_icon'            => '',
            // Specify a custom URL to an icon
            'last_tab'             => '',
            // Force your panel to always open to a specific tab (by id)
            'page_icon'            => 'icon-themes',
            // Icon displayed in the admin panel next to your menu_title
            'page_slug'            => '',
            // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
            'save_defaults'        => true,
            // On load save the defaults to DB before user clicks save or not
            'default_show'         => false,
            // If true, shows the default value next to each field that is not the default value.
            'default_mark'         => '',
            // What to print by the field's title if the value shown is default. Suggested: *
            'show_import_export'   => true,
            // Shows the Import/Export panel when not used as a field.

            // CAREFUL -> These options are for advanced use only
            'transient_time'       => 60 * MINUTE_IN_SECONDS,
            'output'               => true,
            // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
            'output_tag'           => true,
            // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
            'database'             => '',
            // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
            'use_cdn'              => true,
            // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

            // HINTS
            'hints'                => array(
                'icon'          => 'el el-question-sign',
                'icon_position' => 'right',
                'icon_color'    => 'lightgray',
                'icon_size'     => 'normal',
                'tip_style'     => array(
                    'color'   => 'red',
                    'shadow'  => true,
                    'rounded' => false,
                    'style'   => '',
                ),
                'tip_position'  => array(
                    'my' => 'top left',
                    'at' => 'bottom right',
                ),
                'tip_effect'    => array(
                    'show' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'mouseover',
                    ),
                    'hide' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'click mouseleave',
                    ),
                ),
            )
        );

        // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-docs',
            'href'  => 'http://docs.reduxframework.com/',
            'title' => __( 'Documentation', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            //'id'    => 'redux-support',
            'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
            'title' => __( 'Support', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-extensions',
            'href'  => 'reduxframework.com/extensions',
            'title' => __( 'Extensions', 'redux-framework-demo' ),
        );
        // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
        $args['share_icons'][] = array(
            'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
            'title' => 'Visit us on GitHub',
            'icon'  => 'el el-github'
            //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
        );
        $args['share_icons'][] = array(
            'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
            'title' => 'Like us on Facebook',
            'icon'  => 'el el-facebook'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://twitter.com/reduxframework',
            'title' => 'Follow us on Twitter',
            'icon'  => 'el el-twitter'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://www.linkedin.com/company/redux-framework',
            'title' => 'Find us on LinkedIn',
            'icon'  => 'el el-linkedin'
        );

        // Panel Intro text -> before the form
        if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
            if ( ! empty( $args['global_variable'] ) ) {
                $v = $args['global_variable'];
            } else {
                $v = str_replace( '-', '_', $args['opt_name'] );
            }
            $args['intro_text'] = sprintf( __( '', 'redux-framework-demo' ) );
        } else {
            $args['intro_text'] = __( '', 'redux-framework-demo' );
        }

        // Add content after the form.
        $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

        Redux::setArgs( $opt_name, $args );

        /*
         * ---> END ARGUMENTS
         */


        /*
         * ---> START HELP TABS
         */

        $tabs = array(
            array(
                'id'      => 'redux-help-tab-1',
                'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            ),
            array(
                'id'      => 'redux-help-tab-2',
                'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            )
        );
        Redux::setHelpTab( $opt_name, $tabs );

        // Set the help sidebar
        $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
        Redux::setHelpSidebar( $opt_name, $content );


        /*
         * <--- END HELP TABS
         */


        /*
         *
         * ---> START SECTIONS
         *
         */

    /*********************************************
              CAMPOS PERSONALIZADOS
    **********************************************/
        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações Gerais', 'redux-framework-demo' ),
            'id'               => 'configuracoes_site',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Geral', 'redux-framework-demo' ),
                    'id'               => 'config_geral',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_logo',
                            'type'     => 'media',
                            'title'    => __( 'Logo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_button_padrao',
                            'type'     => 'text',
                            'title'    => __( 'Botão "Quero uma simulação" texto', 'redux-framework-demo' ),
                            'desc'    => __( '"Quero uma simulação"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_link_button_padrao',
                            'type'     => 'text',
                            'title'    => __( 'Botão "Quero uma simulação" link', 'redux-framework-demo' ),
                            'desc'    => __( 'Link botão "Quero uma simulação"', 'redux-framework-demo' ),
                        ),

                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações Contato', 'redux-framework-demo' ),
            'id'               => 'configuracoes_contato',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Contato e seção contato', 'redux-framework-demo' ),
                    'id'               => 'config_contato',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_contato',
                            'type'     => 'text',
                            'title'    => __( 'Título seção contato', 'redux-framework-demo' ),
                            'desc'    => __( '"Deixe seu contato"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_contato',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção contato', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_como_prefere',
                            'type'     => 'text',
                            'title'    => __( 'Texto do início', 'redux-framework-demo' ),
                            'desc'    => __( '"Como prefere conversar?"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_telefone_icone',
                            'type'     => 'media',
                            'title'    => __( 'Ícone telefone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_telefone',
                            'type'     => 'text',
                            'title'    => __( 'Telefone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_email_icone',
                            'type'     => 'media',
                            'title'    => __( 'Ícone e-mail', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_email',
                            'type'     => 'text',
                            'title'    => __( 'E-mail', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_videochamada_icone',
                            'type'     => 'media',
                            'title'    => __( 'Ícone vídeochamada', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_videochamada_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título vídeochamada site', 'redux-framework-demo' ),
                            'desc'    => __( '"Agendar vídeo chamada"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_videochamada_link',
                            'type'     => 'text',
                            'title'    => __( 'Link vídeochamada', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_whatsapp_icone',
                            'type'     => 'media',
                            'title'    => __( 'Ícone whatsapp', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_whatsapp_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título whatsapp site', 'redux-framework-demo' ),
                            'desc'    => __( 'Whatsapp', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_whatsapp_numero',
                            'type'     => 'text',
                            'title'    => __( 'Número do whatsapp', 'redux-framework-demo' ),
                            'desc'    => __( '5541991234567', 'redux-framework-demo' ),
                        ),

                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção destaque', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_destaque',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção destaque', 'redux-framework-demo' ),
                    'id'               => 'config_secao_destaque',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_destaque',
                            'type'     => 'text',
                            'title'    => __( 'Título Site', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_descricao_secao_destaque',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_video_secao_destaque',
                            'type'     => 'text',
                            'title'    => __( 'Vídeo seção destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_imagem_video_secao_destaque',
                            'type'     => 'media',
                            'title'    => __( 'Banner vídeo seção destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Ficha técnica', 'redux-framework-demo' ),
            'id'               => 'configuracoes_ficha_tecnica',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar ficha técnica', 'redux-framework-demo' ),
                    'id'               => 'config_ficha_tecnica',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_ficha_tecnica',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '"Ficha técnica"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_1',
                            'type'     => 'text',
                            'title'    => __( 'Primeiro item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"1349.75m²"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_1_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título primeiro item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"Área do terreno"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_2',
                            'type'     => 'text',
                            'title'    => __( 'Segundo item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"15"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_2_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título segundo item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"Pavimentos"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_3',
                            'type'     => 'text',
                            'title'    => __( 'Terceiro item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"01"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_3_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título terceiro item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"Torres"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_4',
                            'type'     => 'text',
                            'title'    => __( 'Quarto item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"100"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_ficha_tecnica_item_4_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título quarto item da ficha', 'redux-framework-demo' ),
                            'desc'    => __( '"Unidade"', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção sobre o terrazzo', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_terrazzo',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção terrazzo', 'redux-framework-demo' ),
                    'id'               => 'config_secao_terrazzo',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_terrazzo',
                            'type'     => 'text',
                            'title'    => __( 'Título seção terrazzo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_subtitulo_secao_terrazzo',
                            'type'     => 'textarea',
                            'title'    => __( 'Subtítulo seção terrazzo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_terrazzo',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção terrazzo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção plantas', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_plantas',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção plantas', 'redux-framework-demo' ),
                    'id'               => 'config_secao_plantas',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_plantas',
                            'type'     => 'text',
                            'title'    => __( 'Título seção plantas', 'redux-framework-demo' ),
                            'desc'    => __( '"Nossas plantas"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_plantas',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção plantas', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção localização', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_localizacao',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção localização', 'redux-framework-demo' ),
                    'id'               => 'config_secao_localizacao',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_localizacao',
                            'type'     => 'text',
                            'title'    => __( 'Título seção localização', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_localizacao',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção localização', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_estabelecimentos_proximos',
                            'type'     => 'text',
                            'title'    => __( 'Título estabelecimentos próximos', 'redux-framework-demo' ),
                            'desc'    => __( '"Estabelecimentos próximos"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_estabelecimentos_proximos_secao_localizacao',
                            'type'     => 'multi_text',
                            'title'    => __( 'Estabelecimentos próximos', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_link_secao_localizacao',
                            'type'     => 'text',
                            'title'    => __( 'Link localização', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção diferenciais', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_diferenciais',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção diferenciais', 'redux-framework-demo' ),
                    'id'               => 'config_secao_diferenciais',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_diferenciais',
                            'type'     => 'text',
                            'title'    => __( 'Título seção diferenciais', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_subtitulo_secao_diferenciais',
                            'type'     => 'textarea',
                            'title'    => __( 'Subtítulo seção diferenciais', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_diferenciais',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção diferenciais', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção aplicativo', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_aplicativo',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção aplicativo', 'redux-framework-demo' ),
                    'id'               => 'config_secao_aplicativo',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_imagem_celular_secao_aplicativo',
                            'type'     => 'media',
                            'title'    => __( 'Imagem celular seção aplicativo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_secao_aplicativo',
                            'type'     => 'text',
                            'title'    => __( 'Título seção aplicativo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_aplicativo',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção aplicativo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_icone_app_store',
                            'type'     => 'media',
                            'title'    => __( 'Ícone loja App Store', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_link_app_store',
                            'type'     => 'text',
                            'title'    => __( 'Link loja App Store', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_icone_play_store',
                            'type'     => 'media',
                            'title'    => __( 'Ícone loja Play Store', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_link_play_store',
                            'type'     => 'text',
                            'title'    => __( 'Link loja Play Store', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Seção sobre a atr', 'redux-framework-demo' ),
            'id'               => 'configuracoes_secao_sobre_atr',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Editar seção sobre a atr', 'redux-framework-demo' ),
                    'id'               => 'config_secao_sobre_atr',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_secao_sobre_atr',
                            'type'     => 'text',
                            'title'    => __( 'Título seção sobre a atr', 'redux-framework-demo' ),
                            'desc'    => __( '"Reiventamos a vida das pessoas na cidade"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_subtitulo_secao_sobre_atr',
                            'type'     => 'textarea',
                            'title'    => __( 'Subtítulo seção sobre a atr', 'redux-framework-demo' ),
                            'desc'    => __( '"Utilizamos o máximo de inovação para entregar apartamentos cheios de criatividade e conforto"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_secao_sobre_atr',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto seção sobre a atr', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_lista_secao_sobre_atr',
                            'type'     => 'multi_text',
                            'title'    => __( 'Lista sobre a atr', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_texto_button_conheca_atr',
                            'type'     => 'text',
                            'title'    => __( 'Texto botão conheça a atr', 'redux-framework-demo' ),
                            'desc'    => __( '"Conheça a atr"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_button_conheca_atr',
                            'type'     => 'text',
                            'title'    => __( 'Link botão conheça a atr', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_imagem_secao_sobre_atr',
                            'type'     => 'media',
                            'title'    => __( 'Imagem seção sobre a atr', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );


    /*********************************************
                 //FUNÇÕES//
    **********************************************/
        Redux::setSection( $opt_name, array(
            'icon'            => 'el el-list-alt',
            'title'           => __( 'Customizer Only', 'redux-framework-demo' ),
            'desc'            => __( '<p class="description">This Section should be visible only in Customizer</p>', 'redux-framework-demo' ),
            'customizer_only' => true,
            'fields'          => array(
                array(
                    'id'              => 'opt-customizer-only',
                    'type'            => 'select',
                    'title'           => __( 'Customizer Only Option', 'redux-framework-demo' ),
                    'subtitle'        => __( 'The subtitle is NOT visible in customizer', 'redux-framework-demo' ),
                    'desc'            => __( 'The field desc is NOT visible in customizer.', 'redux-framework-demo' ),
                    'customizer_only' => true,
                    //Must provide key => value pairs for select options
                    'options'         => array(
                        '1' => 'Opt 1',
                        '2' => 'Opt 2',
                        '3' => 'Opt 3'
                    ),
                    'default'         => '2'
                ),
            )
        ) );




        /*
         * <--- END SECTIONS
         */


        /*
         *
         * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
         *
         */

        /*
        *
        * --> Action hook examples
        *
        */

        // If Redux is running as a plugin, this will remove the demo notice and links
        //add_action( 'redux/loaded', 'remove_demo' );

        // Function to test the compiler hook and demo CSS output.
        // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
        //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

        // Change the arguments after they've been declared, but before the panel is created
        //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

        // Change the default value of a field after it's been set, but before it's been useds
        //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

        // Dynamically add a section. Can be also used to modify sections/fields
        //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

        /**
         * This is a test function that will let you see when the compiler hook occurs.
         * It only runs if a field    set with compiler=>true is changed.
         * */
        if ( ! function_exists( 'compiler_action' ) ) {
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            }
        }

        /**
         * Custom function for the callback validation referenced above
         * */
        if ( ! function_exists( 'redux_validate_callback_function' ) ) {
            function redux_validate_callback_function( $field, $value, $existing_value ) {
                $error   = false;
                $warning = false;

                //do your validation
                if ( $value == 1 ) {
                    $error = true;
                    $value = $existing_value;
                } elseif ( $value == 2 ) {
                    $warning = true;
                    $value   = $existing_value;
                }

                $return['value'] = $value;

                if ( $error == true ) {
                    $return['error'] = $field;
                    $field['msg']    = 'your custom error message';
                }

                if ( $warning == true ) {
                    $return['warning'] = $field;
                    $field['msg']      = 'your custom warning message';
                }

                return $return;
            }
        }

        /**
         * Custom function for the callback referenced above
         */
        if ( ! function_exists( 'redux_my_custom_field' ) ) {
            function redux_my_custom_field( $field, $value ) {
                print_r( $field );
                echo '<br/>';
                print_r( $value );
            }
        }

        /**
         * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
         * Simply include this function in the child themes functions.php file.
         * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
         * so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        if ( ! function_exists( 'dynamic_section' ) ) {
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                    'icon'   => 'el el-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }
        }

        /**
         * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        if ( ! function_exists( 'change_arguments' ) ) {
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }
        }

        /**
         * Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        if ( ! function_exists( 'change_defaults' ) ) {
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }
        }

        /**
         * Removes the demo link and the notice of integrated demo from the redux-framework plugin
         */
        if ( ! function_exists( 'remove_demo' ) ) {
            function remove_demo() {
                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }
        }

