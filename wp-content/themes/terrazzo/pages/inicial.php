<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Terrazzo
 */

$plantas = new WP_Query( array( 'post_type' => 'planta', 'orderby' => 'id', 'posts_per_page' => -1 ) );
$galeria = new WP_Query( array( 'post_type' => 'galeria', 'orderby' => 'id', 'posts_per_page' => -1 ) );
$diferenciais = new WP_Query( array( 'post_type' => 'diferencial', 'orderby' => 'id', 'posts_per_page' => -1 ) );
$imagensSobre = new WP_Query( array( 'post_type' => 'sobre', 'orderby' => 'id', 'posts_per_page' => -1 ) );
$diferencial = new WP_Query( array( 'post_type' => 'diferencial', 'orderby' => 'id', 'posts_per_page' => -1 ) );

$categoria = 'categoriasplantas';
// LISTA AS CATEGORIAS DE PRODUTO
$categoriasPlantas = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

$categoriasPlantas2 = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

get_header(); ?>

<main class="pg pg-inicial">

	<secion class="secao-destaque">
		<h4 class="hidden">Seção destaque</h4>
		<div class="mid-container">
			<div class="row">
				<div class="col-md-5">
					<article>
						<h1 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_destaque']; ?></h1>
						<p><?php echo $configuracao['opt_descricao_secao_destaque']; ?></p>
						<a href="#" class="seta"><img src="<?php echo get_template_directory_uri(); ?>/img/setabaixo.svg" alt="Seta para baixo"></a>
						<div class="div-button-simulacao mobile-element">
							<a href="<?php echo $configuracao['opt_link_button_padrao']; ?>" class="button-simulacao"><?php echo $configuracao['opt_texto_button_padrao']; ?></a>
						</div>
					</article>
				</div>
				<div class="col-md-7">
					<div class="simulacao">
						<div class="video">
							<video poster="<?php echo $configuracao['opt_imagem_video_secao_destaque']['url']; ?>">
								<source src="<?php echo $configuracao['opt_video_secao_destaque']; ?>" type="video/mp4">
								<source src="<?php echo $configuracao['opt_video_secao_destaque']; ?>" type="video/webm">
								<source src="<?php echo $configuracao['opt_video_secao_destaque']; ?>" type="video/ogg">
							</video>
						</div>
						<div class="div-button-simulacao desktop-element">
							<a href="<?php echo $configuracao['opt_link_button_padrao']; ?>" class="button-simulacao"><?php echo $configuracao['opt_texto_button_padrao']; ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</secion>

	<div class="menu-ancora">
		<div class="mid-container">
			<nav class="scrollTop">
				<a href="#secao-plantas">Plantas</a>
				<a href="#secao-galeria">Galeria de Imagens</a>
				<a href="#secao-localizacao">Localização</a>
				<a href="#secao-diferenciais">Lazer e Diferenciais</a>
				<a href="#secao-aplicativo">Aplicativo</a>
				<a href="#secao-sobre">Sobre</a>
				<a href="#secao-contato">Contato</a>
			</nav>
		</div>
	</div>

	<section class="secao-ficha-tecnica">
		<h4 class="hidden">Seção ficha técnica</h4>
		<div class="mid-container">
			<ul>
				<li class="text-left">
					<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_ficha_tecnica']; ?></h2>
				</li>
				<li>
					<p><?php echo $configuracao['opt_ficha_tecnica_item_1']; ?></p>
					<span><?php echo $configuracao['opt_ficha_tecnica_item_1_titulo']; ?></span>
				</li>
				<li>
					<p><?php echo $configuracao['opt_ficha_tecnica_item_2']; ?></p>
					<span><?php echo $configuracao['opt_ficha_tecnica_item_2_titulo']; ?></span>
				</li>
				<li>
					<p><?php echo $configuracao['opt_ficha_tecnica_item_3']; ?></p>
					<span><?php echo $configuracao['opt_ficha_tecnica_item_3_titulo']; ?></span>
				</li>
				<li>
					<p><?php echo $configuracao['opt_ficha_tecnica_item_4']; ?></p>
					<span><?php echo $configuracao['opt_ficha_tecnica_item_4_titulo']; ?></span>
				</li>
			</ul>
		</div>
	</section>

	<section class="secao-sobre">
		<h4 class="hidden">Seção sobre o terrazzo</h4>
		<div class="mid-container">
			<div class="row">
				<div class="col-md-5 desktop-element">
					<figure class="imagem-terrazzo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/terrazzo.jpg" alt="Imagem terrazzo">
						<figcaption class="hidden">Imagem terrazzo</figcaption>
					</figure>
					<div class="button-zoom">
						<span class="maior"><img src="<?php echo get_template_directory_uri(); ?>/img/zoomin.svg" alt="Maior"></span>
						<span class="menor"><img src="<?php echo get_template_directory_uri(); ?>/img/zoomout.svg" alt="Menor"></span>
					</div>
				</div>
				<div class="col-md-7">
					<article>
						<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_terrazzo']; ?></h2>
						<p><?php echo $configuracao['opt_texto_secao_terrazzo']; ?></p>
						<ul class="galeria-sobre-terrazzo">
							
							<?php while($imagensSobre->have_posts()): $imagensSobre->the_post(); ?>
							<li>
								<figure>
									<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full')[0]; ?>" alt="<?php echo get_the_title(); ?>">
									<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
								</figure>
							</li>
							<?php endwhile; wp_reset_query(); ?>

						</ul>
					</article>
				</div>
				<div class="col-md-5 mobile-element">
					<figure class="imagem-terrazzo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/terrazzo.jpg" alt="Imagem terrazzo">
						<figcaption class="hidden">Imagem terrazzo</figcaption>
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section class="secao-plantas" id="secao-plantas">
		<h4 class="hidden">Seção plantas</h4>
		<div class="mid-container">
			<div class="div-info-planta">
				<article>
					<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_plantas']; ?></h2>
					<p><?php echo $configuracao['opt_texto_secao_plantas']; ?></p>
				</article>
			</div>


			<div class="div-plantas">
				<ul class="menu-plantas">
					<?php $k =  0; foreach ($categoriasPlantas as $categoriasPlantas):?>
					<li class="<?php if($k==0){ echo "active";} ?>" data-id="<?php echo $categoriasPlantas->term_id ?>"><?php echo $categoriasPlantas->name ?></li>
				<?php $k++;endforeach;?>
				</ul>

				<?php 
					$j =  0;
					foreach( $categoriasPlantas2 as $categoriasPlantas2 ):

									
						$loopPlantas = new WP_Query(array(
							'post_type'        => 'planta',
							'posts_per_page'   => -1,
							'order' => 'rand',
							'tax_query'        => array(
								array(
									'taxonomy' => 'categoriasplantas',
									'field'    => 'slug',
									'terms'    => $categoriasPlantas2->slug,
									)
								)
							)
						);
				?>
				<div id="carrosselPlantas" class="carrossel-planta owl-carousel <?php if($j==0){ echo "active";} ?> open<?php echo $categoriasPlantas2->term_id ?>">
					
					<?php if($loopPlantas->have_posts()): while($loopPlantas->have_posts()): $loopPlantas->the_post(); ?>
					<div class="item-planta" data-titulo="<?php echo get_the_title(); ?>">
						<div class="sobre-planta">
							<span class="fechar-menu-planta"><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar menu planta"></span>
							<h3 class="titulo-dois"><?php echo get_the_title(); ?></h3>
							<ul>
								<?php $detalhes = rwmb_meta('terrazzo_detalhes_sobre_planta'); foreach($detalhes as $detalhe){
									echo '<li><img src="' . $detalhe['icone'] . '" alt="' . $detalhe['detalhe'] . '">' . $detalhe['detalhe'] . '</li>';
								} ?>
							</ul>
							<div class="div-button-baixar">
								<a href="#" class="button-planta button-baixar">Baixar PDF informativo</a>
							</div>
							<div class="div-button-simulacao">
								<a href="#" class="button-planta button-simulacao">Fazer simulação</a>
							</div>
						</div>
						<div class="imagem-planta">
							<span class="abrir-menu-planta"><img src="<?php echo get_template_directory_uri(); ?>/img/menu-planta.svg" alt="Menu planta"></span>
							<h3 class="titulo-dois"><?php echo get_the_title(); ?></h3>
							<p><?php echo rwmb_meta('terrazzo_metragem_planta'); ?></p>
							<figure>
								<img src="<?php echo rwmb_meta('terrazzo_imagem_planta')['full_url'] ?>" alt="<?php echo get_the_title(); ?>">
								<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
							</figure>
							<div class="button-zoom" id="button-zoom" data-id="<?php echo $categoriasPlantas2->slug ?>">
								<span class="maior"><img src="<?php echo get_template_directory_uri(); ?>/img/zoomin.svg" alt="Maior"></span>
							</div>
						</div>
					</div>
					<?php endwhile; endif; wp_reset_query(); ?>

				</div>
				<?php $j++;endforeach; ?>

			</div>
		</div>
	</section>

	<section class="secao-galeria" id="secao-galeria">
		<h4 class="hidden">SEÇÃO GALERIA DE IMAGENS</h4>
		<div class="mid-container">
			<h2 class="titulo-um">Galeria de imagens</h2>
			<h3 class="titulo-dois">Surpreenda-se e viva conectado com o que realmente te faz bem!</h3>
			<div class="carrossel-galeria owl-carousel">

				<?php $contadorGaleria1 = 0; while($galeria->have_posts()): $galeria->the_post(); ?>
				<figure class="item" data-hash="<?php echo $contadorGaleria1; ?>">
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full')[0]; ?>" alt="<?php echo get_the_title(); ?>">
					<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
				</figure>
				<?php $contadorGaleria1++; endwhile; wp_reset_query(); ?>

			</div>
			<nav class="carrossel-nav-galeria owl-carousel">

				<?php $contadorGaleria2 = 0; while($galeria->have_posts()): $galeria->the_post(); $classeCss = ($contadorGaleria2 === 0) ? 'active' : ''; ?>
				<a href="#<?php echo $contadorGaleria2; ?>" class="<?php echo $classeCss ?>">
					<figure>
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full')[0]; ?>" alt="<?php echo get_the_title(); ?>">
						<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
					</figure>
				</a>
				<?php $contadorGaleria2++; endwhile; wp_reset_query(); ?>

			</nav>
		</div>
	</section>

	<section class="secao-localizacao" id="secao-localizacao">
		<h4 class="hidden">Seção localização</h4>
		<div class="large-container">
			<div class="row">
				<div class="col-md-6 desktop-element">
					<a href="<?php echo $configuracao['opt_link_secao_localizacao']; ?>" class="link-mapa" target="_blank">
						<article class="lugares-proximos">
							<h3 class="titulo-dois"><?php echo $configuracao['opt_titulo_estabelecimentos_proximos']; ?></h3>
							<ul>
								
								<?php $lista_estabelecimentos_proximos = $configuracao['opt_estabelecimentos_proximos_secao_localizacao'];
								foreach ($lista_estabelecimentos_proximos as $estabelecimento){
									echo '<li>' . $estabelecimento . '</li>';
								} ?>

							</ul>
						</article>
					</a>
				</div>
				<div class="col-md-6">
					<article class="sobre-localizacao">
						<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_localizacao']; ?></h2>
						<img src="<?php echo get_template_directory_uri(); ?>/img/mappin.svg" alt="Ícone pin localização" class="icone-localizacao">
						<h5 class="endereco-localizacao"><strong>Rua maria helena 491</strong> São Pedro, São José so Pinhais</h5>
						<p><?php echo $configuracao['opt_texto_secao_localizacao']; ?></p>
					</article>
				</div>
				<div class="col-md-6 mobile-element">
					<a href="<?php echo $configuracao['opt_link_secao_localizacao']; ?>" class="link-mapa" target="_blank">
						<article class="lugares-proximos">
							<h3 class="titulo-dois"><?php echo $configuracao['opt_titulo_estabelecimentos_proximos']; ?></h3>
							<ul>
								
								<?php $lista_estabelecimentos_proximos = $configuracao['opt_estabelecimentos_proximos_secao_localizacao'];
								foreach ($lista_estabelecimentos_proximos as $estabelecimento){
									echo '<li>' . $estabelecimento . '</li>';
								} ?>

							</ul>
						</article>
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="secao-diferenciais" id="secao-diferenciais">
		<h4 class="hidden">Seção diferenciais</h4>
		<div class="large-container">
			<div class="diferenciais before-fundo">
				<div class="mid-container">
					<div class="row">
						<div class="col-md-5">
							<article>
								<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_diferenciais']; ?></h2>
								<h3 class="titulo-dois"><?php echo $configuracao['opt_subtitulo_secao_diferenciais']; ?></h3>
								<p><?php echo $configuracao['opt_texto_secao_diferenciais']; ?></p>
							</article>
						</div>
						<div class="col-md-7">
							<div class="diferencial-terrazzo">
								

								<ul class="menu-diferenciais">
									
									<?php 
										$i = 0; 
										while($diferencial->have_posts()): $diferencial->the_post();
									?>

									<li data-id="<?php echo $i ?>" class="<?php if($i == 0){echo"active";}  ?>"><?php echo get_the_title(); ?></li>
									
									<?php $i++;endwhile; wp_reset_query(); ?>
								
								</ul>

								


								<?php 
									$cont = 0; 
									while($diferencial->have_posts()): $diferencial->the_post(); 
								?>

								<div class="diferencial <?php if($cont == 0){echo"active";}  ?>" id="diferencial<?php echo $cont ?>">
									
									<figure>
										
										<img 
											src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full')[0]; ?>" 
											alt="<?php echo get_the_title() ?>">

										<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
									</figure>
									
									<h2 class="titulo-um"><img src="<?php echo get_template_directory_uri(); ?>/img/diferencial-icone1.png" alt="Estrutura"><?php echo get_the_title() ?></h2>
									<ul>
										<?php 
											$detalhes_sobre_diferencial = rwmb_meta('terrazzo_detalhes_sobre_diferencial'); 

											var_dump($detalhes_sobre_diferencial);

											foreach ($detalhes_sobre_diferencial as $detalhes_sobre_diferencial):
											
										?>
										<li><?php echo $detalhes_sobre_diferencial; ?></li>
										<?php endforeach; ?>
										
									</ul>
								</div>
								<?php $cont++; endwhile; wp_reset_query(); ?>



							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="secao-aplicativo" id="secao-aplicativo">
		<h4 class="hidden">Seção aplicativo</h4>
		<div class="large-container">
			<div class="aplicativo">
				<div class="div-imagem-celular">
					<figure>
						<img src="<?php echo $configuracao['opt_imagem_celular_secao_aplicativo']['url']; ?>" alt="Imagem aplicativo">
						<figcaption class="hidden">Imagem aplicativo</figcaption>
					</figure>
				</div>
				<div class="div-sobre-aplicativo">
					<article class="sobre-aplicativo">
						<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_aplicativo']; ?></h2>
						<p><?php echo $configuracao['opt_texto_secao_aplicativo']; ?></p>
						<div class="icones-app-store">
							<a href="<?php echo $configuracao['opt_link_app_store']; ?>"><img src="<?php echo $configuracao['opt_icone_app_store']['url']; ?>" alt="App Store"></a>							<a href="<?php echo $configuracao['opt_link_play_store']; ?>"><img src="<?php echo $configuracao['opt_icone_play_store']['url']; ?>" alt="Google Play"></a>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="secao-sobre-atr" id="secao-sobre">
		<h4 class="hidden">Seção sobre a ATR</h4>
		<div class="large-container">
			<div class="sobre-atr before-fundo">
				<div class="row">
					<div class="col-md-8">
						<article>
							<img src="<?php echo get_template_directory_uri(); ?>/img/logoatr.png" alt="Detalhe atr">
							<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_sobre_atr']; ?></h2>
							<p class="titulo-dois"><?php echo $configuracao['opt_subtitulo_secao_sobre_atr']; ?></p>
							<div class="descricao">
								<span><?php echo $configuracao['opt_texto_secao_sobre_atr']; ?></span>
								<ul>

									<?php $lista_sobre_atr = $configuracao['opt_lista_secao_sobre_atr'];
									foreach ($lista_sobre_atr as $item_sobre_atr){
										echo '<li>' . $item_sobre_atr . '</li>';
									} ?>

								</ul>
							</div>
							<a href="<?php echo $configuracao['opt_button_conheca_atr']; ?>" class="button-simulacao button-conheca-atr" target="_blank"><?php echo $configuracao['opt_texto_button_conheca_atr']; ?></a>
						</article>
					</div>
					<div class="col-md-4">
						<figure>
							<img src="<?php echo $configuracao['opt_imagem_secao_sobre_atr']['url']; ?>" alt="Imagem sobre ATR">
							<figcaption class="hidden">Imagem sobre ATR</figcaption>
						</figure>
						<span class="logo-atr logo-atr-right"><img src="<?php echo get_template_directory_uri(); ?>/img/atr1.png" alt="Logo atr"></span>
						<span class="logo-atr logo-atr-left"><img src="<?php echo get_template_directory_uri(); ?>/img/atr2.png" alt="Logo atr"></span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="secao-contato" id="secao-contato">
		<h4 class="hidden">Seção contato</h4>
		<div class="large-container">
			<div class="row">
				<div class="col-md-4">
					<article class="article-contato">
						<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_contato']; ?></h2>
						<p><?php echo $configuracao['opt_texto_secao_contato']; ?></p>
						<div class="modo-contato">
							<p class="titulo-dois">Como prefere ser contatado?</p>
							<label for="radio-whatsapp">
								<input type="radio" name="modo-contato" id="radio-whatsapp">
								<span class="span-label-radio">Whatsapp</span>
							</label>
							<label for="radio-email">
								<input type="radio" name="modo-contato" id="radio-email">
								<span class="span-label-radio">E-mail</span>
							</label>
							<label for="radio-telefone">
								<input type="radio" name="modo-contato" id="radio-telefone">
								<span class="span-label-radio">Telefone</span>
							</label>
						</div>
					</article>
				</div>
				<div class="col-md-8">
					<div class="secao-inputs">
						<?php echo do_shortcode('[contact-form-7 id="72" title="Deixe seu contato"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="div-contato-fixo">
		<div class="large-container">
			<div class="row">
				<div class="col-sm-3">
					<p>Como prefere conversar?</p>
				</div>
				<div class="col-sm-6">
					<ul>
						<li>
							<a href="tel:<?php echo $configuracao['opt_telefone']; ?>" class="desktop-element"><?php echo $configuracao['opt_telefone']; ?></a>
							<a href="tel:<?php echo $configuracao['opt_telefone']; ?>" class="mobile-element"><img src="<?php echo $configuracao['opt_telefone_icone']['url']; ?>" alt="Telefone"></a>
						</li>
						<li>
							<a href="mailto:<?php echo $configuracao['opt_email']; ?>" class="desktop-element"><?php echo $configuracao['opt_email']; ?></a>
							<a href="mailto:<?php echo $configuracao['opt_email']; ?>" class="mobile-element"><img src="<?php echo $configuracao['opt_email_icone']['url']; ?>" alt="Email"></a>
						</li>
						<li>
							<a href="<?php echo $configuracao['opt_videochamada_link']; ?>" class="desktop-element"><?php echo $configuracao['opt_videochamada_titulo']; ?></a>
							<a href="<?php echo $configuracao['opt_videochamada_link']; ?>" class="mobile-element"><img src="<?php echo $configuracao['opt_videochamada_icone']['url']; ?>" alt="Vídeo chamada"></a>
						</li>
						<li>
							<a href="https://api.whatsapp.com/send?phone=<?php echo $configuracao['opt_whatsapp_numero']; ?>&text=Ol%C3%A1!" class="desktop-element"><?php echo $configuracao['opt_whatsapp_titulo']; ?></a>
							<a href="https://api.whatsapp.com/send?phone=<?php echo $configuracao['opt_whatsapp_numero']; ?>&text=Ol%C3%A1!" class="mobile-element"><img src="<?php echo $configuracao['opt_whatsapp_icone']['url']; ?>" alt="Whatsapp"></a>
						</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<div class="div-button-simulacao">
						<a href="<?php echo $configuracao['opt_link_button_padrao']; ?>" class="button-simulacao"><?php echo $configuracao['opt_texto_button_padrao']; ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="div-pop-up-simulacao">
		<div class="container-pop-up-simulacao">
			<div class="pop-up-simulacao">
				<div class="row">
					<div class="col-md-5">
						<article class="article-contato">
							<h2 class="titulo-um"><?php echo $configuracao['opt_titulo_secao_contato']; ?></h2>
							<p><?php echo $configuracao['opt_texto_secao_contato']; ?></p>
							<div class="modo-contato">
								<p class="titulo-dois">Como prefere ser contatado?</p>
								<label for="radio-whatsapp-pop-up">
									<input type="radio" name="modo-contato" id="radio-whatsapp-pop-up">
									<span class="span-label-radio">Whatsapp</span>
								</label>
								<label for="radio-email-pop-up">
									<input type="radio" name="modo-contato" id="radio-email-pop-up">
									<span class="span-label-radio">E-mail</span>
								</label>
								<label for="radio-telefone-pop-up">
									<input type="radio" name="modo-contato" id="radio-telefone-pop-up">
									<span class="span-label-radio">Telefone</span>
								</label>
							</div>
						</article>
					</div>
					<div class="col-md-7">
						<div class="secao-inputs">
							<span class="fechar-pop-up-simulacao"><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar pop up simulação"></span>
							<?php echo do_shortcode('[contact-form-7 id="71" title="Quero uma simulação"]'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<?php 
       	//POP UP
		include (TEMPLATEPATH . '/template/popUp.php'); 
	?>

	<div class="div-pop-up-imagem">
		<span class="fechar-pop-up-imagem"><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar pop up simulação"></span>
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/terrazzo.jpg" alt="Imagem terrazzo">
			<figcaption class="hidden">Imagem terrazzo</figcaption>
		</figure>
	</div>

</main>