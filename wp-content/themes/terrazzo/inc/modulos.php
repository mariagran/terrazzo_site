<?php

	function base_modulos () {

		//TIPOS DE CONTEÚDO
		conteudos_terrazzo();

		//TAXONOMIA
		taxonomia_terrazzo();

		//META BOXES
		metaboxes_terrazzo();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	
		function conteudos_terrazzo (){
		
			// CUSTOM POST TYPE DESTAQUES
			tipoDestaque();

			// CUSTOM POST TYPE PLANTAS
			tipoPlantas();

			// CUSTOM POST TYPE SERVIÇO
			tipoDiferenciais();

			// CUSTOM POST TYPE SERVIÇO
			tipoGaleria();

			// CUSTOM POST TYPE EVENTOS
			tipoSobreTerrazzo();

		}

	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(

									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE PROJETOS
		function tipoPlantas() {

			$rotulosPlantas = array(
									'name'               => 'Planta',
									'singular_name'      => 'planta',
									'menu_name'          => 'Plantas',
									'name_admin_bar'     => 'Plantas',
									'add_new'            => 'Adicionar planta',
									'add_new_item'       => 'Adicionar novo planta',
									'new_item'           => 'Novo planta',
									'edit_item'          => 'Editar planta',
									'view_item'          => 'Ver planta',
									'all_items'          => 'Todos os plantas',
									'search_items'       => 'Buscar planta',
									'parent_item_colon'  => 'Dos plantas',
									'not_found'          => 'Nenhum planta cadastrado.',
									'not_found_in_trash' => 'Nenhum planta na lixeira.'
								);

			$argsPlantas 	= array(
									'labels'             => $rotulosPlantas,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-home',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'plantas' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('planta', $argsPlantas);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoDiferenciais() {

			$rotulosDiferenciais = array(
									'name'               => 'Diferenciais',
									'singular_name'      => 'diferencial',
									'menu_name'          => 'Diferenciais',
									'name_admin_bar'     => 'Diferenciais',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo diferencial',
									'new_item'           => 'Novo diferencial',
									'edit_item'          => 'Editar diferencial',
									'view_item'          => 'Ver diferencial',
									'all_items'          => 'Todos os diferenciais',
									'search_items'       => 'Buscar diferenciais',
									'parent_item_colon'  => 'Dos diferenciais',
									'not_found'          => 'Nenhum diferencial cadastrado.',
									'not_found_in_trash' => 'Nenhum diferencial na lixeira.'
								);

			$argsDiferenciais 	= array(
									'labels'             => $rotulosDiferenciais,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-forms',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'diferenciais' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('diferencial', $argsDiferenciais);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoGaleria() {

			$rotulosGaleria = array(

									'name'               => 'Galeria',
									'singular_name'      => 'galeria',
									'menu_name'          => 'Galeria',
									'name_admin_bar'     => 'Galeria',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova imagem',
									'new_item'           => 'Novo imagem',
									'edit_item'          => 'Editar imagem',
									'view_item'          => 'Ver imagem',
									'all_items'          => 'Todas as imagens',
									'search_items'       => 'Buscar imagem',
									'parent_item_colon'  => 'Das imagens',
									'not_found'          => 'Nenhuma imagem cadastrada',
									'not_found_in_trash' => 'Nenhuma imagem na lixeira'

								);

			$argsGaleria 	= array(

									'labels'             => $rotulosGaleria,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-format-gallery',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'galeria' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('galeria', $argsGaleria);

		}

		// CUSTOM POST TYPE SOBRE
		function tipoSobreTerrazzo() {

			$rotulosSobreTerrazzo = array(

									'name'               => 'Imagens sobre',
									'singular_name'      => 'imagem_sobre',
									'menu_name'          => 'Imagens sobre',
									'name_admin_bar'     => 'Imagens sobre',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova imagem',
									'new_item'           => 'Nova imagem',
									'edit_item'          => 'Editar imagem',
									'view_item'          => 'Ver imagem',
									'all_items'          => 'Todas as imagens',
									'search_items'       => 'Buscar imagens',
									'parent_item_colon'  => 'Das imagens',
									'not_found'          => 'Nenhuma imagem cadastrada.',
									'not_found_in_trash' => 'Nenhuma imagem na lixeira.'
								);

			$argsTerrazzo 	= array(

									'labels'             => $rotulosSobreTerrazzo,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'sobre' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('sobre', $argsTerrazzo);

		}

	/****************************************************
	* META BOXES
	*****************************************************/

		function metaboxes_terrazzo(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'terrazzo_';

				// METABOX  DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxDestaques',
					'title'			=> 'Detalhes',
					'pages' 		=> array('destaque'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Link banner: ',
							'id'    => "{$prefix}destaque_banner",
							'type'  => 'text'
						), 

						
					),

				);

				// METABOX  PROJETO
				$metaboxes[] = array(
					'id'			=> 'metaboxPlantas',
					'title'			=> 'Detalhes planta',
					'pages' 		=> array('planta'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'   => 'Metragem da planta: ',
							'id'     => "{$prefix}metragem_planta",
							'type'   => 'text'
						),

						array(
						    'id'      => "{$prefix}diferenciais_planta",
						    'name'    => 'Diferenciais',
						    'type'    => 'fieldset_text',

						    // Options: array of key => Label for text boxes
						    // Note: key is used as key of array of values stored in the database
						    'options' => array(
						        'icone'			=> 'Url - Ícone diferencial',
						        'diferencial'	=> 'Título diferencial',
						    ),
						    // Is field cloneable?
						    'clone'	=> true,
						),

						array(
						    'id'      => "{$prefix}detalhes_sobre_planta",
						    'name'    => 'Detalhes',
						    'type'    => 'fieldset_text',

						    // Options: array of key => Label for text boxes
						    // Note: key is used as key of array of values stored in the database
						    'options' => array(
						        'icone'			=> 'Url - Ícone detalhe',
						        'detalhe'	=> 'Título detalhe',
						    ),
						    // Is field cloneable?
						    'clone'	=> true,
						),

						array(
							'name'   => 'Imagem planta: ',
							'id'     => "{$prefix}imagem_planta",
							'type'   => 'single_image',
						),

						array(
							'name'   => 'Imagem 3D planta: ',
							'id'     => "{$prefix}imagem_3d_planta",
							'type'   => 'single_image',
						),

					),

				);

				// METABOX  SERVIÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxDiferenciais',
					'title'			=> 'Detalhes do diferencial',
					'pages' 		=> array('diferencial'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'        => 'Detalhes do diferencial',
							'id'          => "{$prefix}detalhes_sobre_diferencial",
							'desc'        => '',
							'type'        => 'text',
							'clone'       => true,
						),

						
					),

				);

				return $metaboxes;

			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomia_terrazzo() {

			//taxonomiaServicos();

			taxonomiaPlanta();

		}

		function taxonomiaPlanta() {

			$rotulosTaxonomiaPlanta = array(
												'name'              => 'Categorias de planta',
												'singular_name'     => 'Categorias de plantas',
												'search_items'      => 'Buscar categoria do planta',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do planta',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias plantas',
											);

			$argsTaxonomiaPlanta 		= array(

												'hierarchical'      => true,
												'labels'            => $rotulosTaxonomiaPlanta,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-planta' ),
											);

			register_taxonomy( 'categoriasplantas', array( 'planta' ), $argsTaxonomiaPlanta);

		}

  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'base_modulos');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		//add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {
	    	base_modulos();
	   		flush_rewrite_rules();
		}
		register_activation_hook( __FILE__, 'rewrite_flush' );