<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Terrazzo
 */

// wp_nav_menu(
// 	array(
// 		'theme_location' => 'menu-1',
// 		'menu_id'        => 'primary-menu',
// 	)
// );

global $configuracao;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() . '/img/favicon.ico'; ?>" /> 

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<header>
		<div class="mid-container">
			<div class="row">
				<!-- LOGO -->
				<div class="col-sm-3">
					<a class="logo" href="#">
						<figure>
							<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="Logo Terrazzo">
							<figcaption class="hidden">Logo Terrazzo</figcaption>
						</figure>
					</a>
				</div>
				<!-- MENU -->	
				<div class="col-sm-9">
					<nav class="navbar navbar-expand-lg">
						<div class="navbar-collapse">
							<ul class="navbar-ul">
								<li>
									<a href="#"><img src="<?php echo $configuracao['opt_telefone_icone']['url'] ?>" alt="Ícone"></a>
								</li>
								<li>
									<a href="#"><img src="<?php echo $configuracao['opt_email_icone']['url'] ?>" alt="Ícone"></a>
								</li>
								<li>
									<a href="#"><img src="<?php echo $configuracao['opt_videochamada_icone']['url'] ?>" alt="Ícone"></a>
								</li>
								<li>
									<a href="#"><img src="<?php echo $configuracao['opt_whatsapp_icone']['url'] ?>" alt="Ícone"></a>
								</li>
								<li class="button-menu">
									<button>
										<img src="<?php echo get_template_directory_uri(); ?>/img/menu.svg" alt="Ícone menu">
									</button>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>