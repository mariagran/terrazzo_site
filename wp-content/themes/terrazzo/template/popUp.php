<?php 
	$categoriasPlantas3 = get_terms( $categoria, array(
		'orderby'    => 'count',
		'hide_empty' => true,
		'parent'	 => '',
		'order'      => 'DESC',
	));

	foreach( $categoriasPlantas3 as $categoriasPlantas3 ):

					
		$loopPlantas2 = new WP_Query(array(
			'post_type'        => 'planta',
			'posts_per_page'   => -1,
			'order' => 'rand',
			'tax_query'        => array(
				array(
					'taxonomy' => 'categoriasplantas',
					'field'    => 'slug',
					'terms'    => $categoriasPlantas3->slug,
					)
				)
			)
		);



?>
<div class="pop-up-plantas open<?php echo $categoriasPlantas3->slug; ?>">
	<div class="div-plantas">
		<div class="carrossel-planta owl-carousel">
			<?php if($loopPlantas2->have_posts()): while($loopPlantas2->have_posts()): $loopPlantas2->the_post(); ?>
				
				<div class="item-planta" data-titulo="<?php echo get_the_title(); ?>">
					<div class="sobre-planta">
						<h3 class="titulo-dois"><?php echo get_the_title(); ?></h3>
						<ul>
							<?php $detalhes = rwmb_meta('terrazzo_detalhes_sobre_planta'); foreach($detalhes as $detalhe){
								echo '<li><img src="' . $detalhe['icone'] . '" alt="' . $detalhe['detalhe'] . '">' . $detalhe['detalhe'] . '</li>';
							} ?>
						</ul>
						<div class="buttons-planta">
							<div class="div-button-baixar">
								<a href="#" class="button-planta button-baixar">Baixar PDF informativo</a>
							</div>
							<div class="div-button-simulacao">
								<a href="#" class="button-planta button-simulacao">Fazer simulação</a>
							</div>
						</div>
					</div>
					<div class="imagem-planta">
						<h3 class="titulo-dois"><?php echo get_the_title(); ?></h3>
						<p><?php echo rwmb_meta('terrazzo_metragem_planta'); ?></p>
						<figure>
							<img src="<?php echo rwmb_meta('terrazzo_imagem_planta')['full_url'] ?>" alt="<?php echo get_the_title(); ?>">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
						</figure>
						<div class="button-zoom">
							<span class="menor"><img src="<?php echo get_template_directory_uri(); ?>/img/zoomout.svg" alt="Menor"></span>
						</div>
					</div>
				</div>
			<?php endwhile; endif; wp_reset_query(); ?>
		</div>
	</div>
</div>
<?php endforeach; ?>